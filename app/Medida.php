<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Medida extends Model
{
    protected $table = "medida";
    protected function user(){
      return $this->belongsTo(User::class, 'id_cliente');
    }
    protected $fillable = [
        'id_cliente', 'LDpieI', 'ADArcoI', 'ADTalonI', 'MDBotonI',  'LDpieD', 'ADArcoD', 'ADTalonD', 'MDBotonD'
    ];
}
