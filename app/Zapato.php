<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Zapato extends Model
{
    protected $table = "zapato";

    protected $fillable = [
      'id_modelo', 'material', 'color', 'id_imagen'
    ];

    protected function modelo(){
      return $this->belongsTo(Modelo::class, 'id_modelo');
    }
    protected function imagen(){
      return $this->belongsTo(Imagen::class, 'id_imagen');
    }
}
