<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plantilla extends Model
{
    protected $table = "plantilla";
    protected function medida(){
      return $this->belongsTo(Medida::class, 'id_medida');
    }
}
