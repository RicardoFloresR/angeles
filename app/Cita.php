<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cita extends Model
{
    protected $table = "cita";
    protected function usuario(){
      return $this->belongsTo(User::class, 'id_cliente');
    }
}
