<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    protected $table = "producto";

    protected $fillable = [
      'nombre', 'costo', 'precio', 'stock', 'id_imagen'
    ];

    protected function imagen(){
      return $this->belongsTo(Imagen::class, 'id_imagen');
    }
}
