<?php

namespace App\Http\Controllers;

use Auth;
use App\Trabajo;
use App\Estado;
use App\User;
use App\Modelo;
use App\Zapato;
use App\Cita;
use App\Producto;
use App\Material;
use App\Proveedor;
use App\Medida;
use App\Imagen;
use Illuminate\Support\Facades\Validator;
use DateTime;

use Illuminate\Http\Request;

class ManagerController extends Controller
{

    public function encargado_general(){
      if(Auth::check()){
        $trabajos = Trabajo::all();
        return view('encargado/encargado-general', compact('trabajos'));
      }
      else {
        return redirect('/iniciar-sesion');
      }
    }

    public function agregar_trabajo(){
      $medidas = Medida::all();
      $zapatos = Zapato::all();
      $tecnicos = User::where('id_rol', 3)->get();
      return view('encargado/encargado-nuevo-trabajo', compact('medidas', 'zapatos', 'tecnicos'));
    }

    public function new_work(){
      $data = request()->validate([
        // Reglas de validacion
        'id_medida' => 'required|integer|exists:medida,id',
        'id_tecnico' => 'required|integer|exists:users,id',
        'id_zapato' => 'required|integer|exists:zapato,id',
        'costo' => 'required|numeric',
        'precio' => 'required|numeric'
      ], [
        // Mensajes a enviar si no se cumple alguna regla
        'id_medida.integer' => 'Cliente: Cliente no valido, por favor seleccione uno correctamente.',
        'id_medida.required' => 'Cliente: Seleccione un cliente.',
        'id_tecnico.integer' => 'Técnico: Técnico no valido, por favor seleccione uno correctamente.',
        'id_tecnico.required' => 'Técnico: Seleccione un técnico.',
        'id_zapato.integer' => 'Zapato: Zapato no valido, por favor seleccione uno correctamente.',
        'id_zapato.required' => 'Medida: Seleccione un modelo de zapato.',
        'costo.required' => 'Costo: Introduzca una cantidad.',
        'costo.numeric' => 'Costo: Cantidad no valida.',
        'precio.required' => 'Precio: Introduzca una cantidad.',
        'precio.numeric' => 'Precio: Cantidad no valida.'
      ]);
      Trabajo::create([
        'id_tecnico' => $data['id_tecnico'],
        'id_medida' => $data['id_medida'],
        'id_zapato' => $data['id_zapato'],
        'costo' => $data['costo'],
        'precio' => $data['precio'],
        'id_estado' => 4,
      ]);
      return redirect('/encargado/general');
    }

    public function encargado_citas(){
      if(Auth::check()){
        $citas= Cita::all();
        return view('encargado/encargado-citas', compact('citas'));
      }
      else{
        return redirect('/iniciar-sesion');
      }
    }

    public function encargado_productos(){
      if(Auth::check()){
        $productos = Producto::all();
        $zapatos = Zapato::all();
        return view ('encargado/encargado-productos', compact('productos', 'zapatos'));
      }
      else{
        return redirect('/iniciar-sesion');
      }

    }

    public function agregar_producto(){
      if(Auth::check()){
        return view ('encargado/encargado-nuevo-producto');
      }
      return redirect('/');
    }

    public function add_product(){
      // Validar datos para crear producto
      if(Auth::check()){
        $data = request()->validate([
          'nombre' => 'required|string',
          'costo' => 'required|numeric',
          'precio' => 'required|numeric',
          'stock' => 'required|numeric',
          'imagen' => 'required|image'
        ], [
          'nombre.required' => 'Nombre: Introduzca un nombre para el producto.',
          'costo.required' => 'Costo: Introduzca una cantidad.',
          'costo.numeric' => 'Costo: Cantidad no valida.',
          'precio.required' => 'Precio: Introduzca una cantidad.',
          'precio.numeric' => 'Precio: Cantidad no valida.',
          'stock.required' => 'Stock: Introduzca una cantidad.',
          'stock.numeric' => 'Stock: Cantidad no valida.',
          'imagen.required' => 'Imagen: Seleccione un archivo en su equipo.',
          'imagen.image' => 'Imagen: Formato de archivo no valido.'
        ]);
        // NOTA: el formulario de origen debe tener el atributo enctype="multipart/form-data"
        // de otro modo, los input file no funcionaran

        // Almacenar imagen en una variable
        $file = $data['imagen'];
        // Cadena auxialiar para no repetir nombres
        $aux = new DateTime();
        $date = $aux->format('Y-m-d-H-i-s');
        //Nombre de la Imagen
        $fileName = $date.'_'.$file->getClientOriginalName();
        // Direccion de la imagen dentro del sistema
        $fileDirectory = 'img/'.$fileName;
        // Crear y almacenar NOMBRE de la imagen con su respectivo
        // directorio en la BDD
        Imagen::create([
          'url' => $fileDirectory
        ]);
        // Guardar imagen en el servidor
        $file->move('img', $fileName);
        // Seleccionar la imagen recien agregada
        $currentImage = Imagen::where('url', $fileDirectory)->first();
        // Crear producto y asociar imagen
        Producto::create([
          'nombre' => $data['nombre'],
          'costo' => $data['costo'],
          'precio' => $data['precio'],
          'stock' => $data['stock'],
          'id_imagen' => $currentImage->id
        ]);
        return redirect('/encargado/productos');
      }
      return redirect('/iniciar-sesion');
    }

    public function editar_producto($id){
      $producto = Producto::where('id', $id)->first();
      return view('encargado/encargado-editar-producto', compact('producto'));
    }

    public function edit_product($id){
      $data = request()->validate([
        'nombre' => 'required|string',
        'costo' => 'required|numeric',
        'precio' => 'required|numeric',
        'imagen' => 'nullable|image'
      ], [
        'nombre.required' => 'Nombre: Introduzca un nombre para el producto.',
        'costo.required' => 'Costo: Introduzca una cantidad.',
        'costo.numeric' => 'Costo: Cantidad no valida.',
        'precio.required' => 'Precio: Introduzca una cantidad.',
        'precio.numeric' => 'Precio: Cantidad no valida.',
        'imagen.image' => 'Imagen: Formato de archivo no valido.'
      ]);
      // Seleccionar producto a editar
      $producto = Producto::where('id', $id)->first();
      // Si introdujo una nueva imagen, entonces:
      if(!$data['imagen'] == null){
        // Seleccionar la imagen vieja
        $oldFile = Imagen::where('url', $producto->imagen->url)->first();
        // Eliminar imagen correspondiente en el servidor
        unlink(public_path($producto->imagen->url));
        // Subir registro de nueva imagen
        $newFile = $data['imagen'];
        $aux = new DateTime();
        $date = $aux->format('Y-m-d-H-i-s');
        $newFileName = $date.'_'.$newFile->getClientOriginalName();
        Imagen::create([
          'url' => 'img/'.$newFileName
        ]);
        // Guardar imagen en el servidor
        $newFile->move('img',$newFileName);
        // seleccionar nueva imagen
        $currentNewFile = Imagen::where('url', 'img/'.$newFileName)->first();
        // Actualizar datos del producto excepto id_imagen
        $producto->update([
          'nombre' => $data['nombre'],
          'costo' => $data['costo'],
          'precio' => $data['precio'],
          'id_imagen' => $currentNewFile->id
        ]);
        // Eliminar registro de la vieja imagen en la BDD
        $oldFile->delete();
        return redirect('encargado/productos');
      }
      // Si no se introdujo una nueva imagen
      else {
        $producto->update([
          'nombre' => $data['nombre'],
          'costo' => $data['costo'],
          'precio' => $data['precio']
        ]);
        return redirect('/encargado/productos');
      }
    }

    public function delete_product($id){
      // Seleccionar producto
      $producto = Producto::findOrFail($id);
      // Obtener imagen asignada en la BDD
      $fileDirectory = $producto->imagen;
      // Borrar imagen seleccionada
      unlink(public_path($fileDirectory->url));
      // Borrar producto de la BDD
      $producto->delete();
      // Borrar registro de la imagen en la BDD
      $fileToDelete = Imagen::findOrFail($fileDirectory->id);
      $fileToDelete->delete();
      return redirect('/encargado/productos');
    }

    public function add_model(){
      $data = request()->validate([
        'nombre' => 'required|string'
      ]);
      Modelo::create([
        'nombre' => $data['nombre']
      ]);
      return redirect('/encargado/productos');
    }

    public function nuevo_zapato(){
      $modelos = Modelo::all();
      return view('encargado/encargado-nuevo-zapato', compact('modelos'));
    }

    public function new_shoe(){
      $data = request()->validate([
        'modelo' => 'required|integer|exists:modelo,id',
        'material' => 'required|string',
        'color' => 'required|string',
        'imagen' => 'required|image'
      ], [
        'modelo.required' => 'Modelo: Seleccione un modelo.',
        'modelo.integer' => 'Modelo: Seleccione un modelo valido.',
        'material.required' => 'Material: Introduzca un nombre de material.',
        'imagen.required' => 'Imagen: Seleccione una imagen.',
        'imagen.image' => 'Imagen: Formato de archivo no valido.'
      ]);
      $file = $data['imagen'];
      // Cadena auxialiar para no repetir nombres
      $aux = new DateTime();
      $date = $aux->format('Y-m-d-H-i-s');
      //Nombre de la Imagen
      $fileName = $date.'_'.$file->getClientOriginalName();
      // Direccion de la imagen dentro del sistema
      $fileDirectory = 'img/'.$fileName;
      // Crear y almacenar NOMBRE de la imagen con su respectivo
      // directorio en la BDD
      Imagen::create([
        'url' => $fileDirectory
      ]);
      // Guardar imagen en el servidor
      $file->move('img', $fileName);
      // Seleccionar la imagen recien agregada
      $currentImage = Imagen::where('url', $fileDirectory)->first();
      // Crear producto y asociar imagen
      Zapato::create([
        'id_modelo' => $data['modelo'],
        'material' => $data['material'],
        'color' => $data['color'],
        'id_imagen' => $currentImage->id
      ]);
      return redirect('/encargado/productos');
    }

    public function editar_zapato($id){
      $zapato = Zapato::findOrFail($id);
      $modelos = Modelo::all();
      return view('encargado/encargado-editar-zapato', compact('zapato', 'modelos'));
    }

    public function edit_shoe($id){
      $data = request()->validate([
        'modelo_id' => 'required|integer|exists:modelo,id',
        'material' => 'required|string',
        'color' => 'required|string',
        'imagen' => 'nullable|image'
      ], [
        'modelo_id.required' => 'Modelo: Seleccione un modelo.',
        'modelo_id.integer' => 'Modelo: Modelo no valido.',
        'material.required' => 'Material: Introduzca el nombre del material.',
        'color.required' => 'Color: Introduzca el color del zapato.',
        'imagen.image' => 'Imagen: Formato de archivo no valido.'
      ]);
      $zapato = Zapato::findOrFail($id);
      if(!$data['imagen'] == null){
        // Seleccionar la imagen vieja
        $oldFile = Imagen::where('url', $zapato->imagen->url)->first();
        // Eliminar imagen correspondiente en el servidor
        unlink(public_path($zapato->imagen->url));
        // Subir registro de nueva imagen
        $newFile = $data['imagen'];
        $aux = new DateTime();
        $date = $aux->format('Y-m-d-H-i-s');
        $newFileName = $date.'_'.$newFile->getClientOriginalName();
        Imagen::create([
          'url' => 'img/'.$newFileName
        ]);
        // Guardar imagen en el servidor
        $newFile->move('img',$newFileName);
        // seleccionar nueva imagen
        $currentNewFile = Imagen::where('url', 'img/'.$newFileName)->first();
        // Actualizar datos del producto excepto id_imagen
        $zapato->update([
          'modelo_id' => $data['modelo_id'],
          'material' => $data['material'],
          'color' => $data['color'],
          'id_imagen' => $currentNewFile->id
        ]);
        // Eliminar registro de la vieja imagen en la BDD
        $oldFile->delete();
        return redirect('encargado/productos');
      }
      else {
        $zapato->update([
          'modelo_id' => $data['modelo_id'],
          'material' => $data['material'],
          'color' => $data['color']
        ]);
        return redirect('encargado/productos');
      }
    }

    public function delete_shoe($id){
      // Seleccionar zapato
      $zapato = Zapato::findOrFail($id);
      // Obtener imagen asignada en la BDD
      $fileDirectory = $zapato->imagen;
      // Borrar imagen seleccionada
      unlink(public_path($fileDirectory->url));
      // Borrar producto de la BDD
      $zapato->delete();
      // Borrar registro de la imagen en la BDD
      $fileToDelete = Imagen::findOrFail($fileDirectory->id);
      $fileToDelete->delete();
      return redirect('/encargado/productos');
    }

    public function encargado_material(){
      if(Auth::check()){
        $materiales = Material::all();
        $proveedores = Proveedor::all();
        return view('encargado/encargado-material', compact('materiales','proveedores'));
      }
      else {
        return redirect('/iniciar-sesion');
      }
    }

    public function agregar_material(){
      $proveedores = Proveedor::all();
      return view('encargado/encargado-nuevo-material', compact('proveedores'));
    }

    public function add_material(){
      $data = request()->validate([
        'id_proveedor' => 'required|integer|exists:proveedor,id',
        'nombre' => 'required|string',
        'stock' => 'required|integer',
        'unidad_de_medida' => 'required|string'
      ], [
        'id_proveedor.required' => 'Proveedor: Seleccione un proveedor.',
        'id_proveedor.integer' => 'Proveedor: Proveedor no valido',
        'nombre.required' => 'Nombre: Introduzca un nombre para el material.' ,
        'stock.required' => 'Stock inicial: Introduzca una cantidad.',
        'stock.integer' => 'Stock inicial: Cantidad no valida.',
        'unidad_de_medida.required' => 'Unidad de medida: Introduzca una unidad de medida.'
      ]);
      Material::create($data);
      return redirect('/encargado/materiales');
    }

    public function editar_material($id){
      $material = Material::findOrFail($id);
      $proveedores = Proveedor::all();

      return view('encargado/encargado-editar-material', compact('material', 'proveedores'));
    }

    public function edit_material($id){
      $material = Material::findOrFail($id);
      $data = request()->validate([
        'id_proveedor' => 'required|integer|exists:proveedor,id',
        'nombre' => 'required|string',
        'unidad_de_medida' => 'required|string'
      ], [
        'id_proveedor.required' => 'Proveedor: Seleccione un proveedor.',
        'id_proveedor.integer' => 'Proveedor: Proveedor no valido',
        'nombre.required' => 'Nombre: Introduzca un nombre.',
        'unidad_de_medida.required' => 'Unidad de medida: Introduzca una unidad de medida'
      ]);
      $material->update($data);
      return redirect('/encargado/materiales');
    }

    public function encargado_clientes(){
      if(Auth::check()){
        $clientes = User::where('id_rol','=',1)->whereNotNull('emailV')-> get();
        $empleados= User::where('id_rol','=',3)->get();
        $solicitudes= User::whereNull('emailV')->get();
        return view('encargado/encargado-usuarios', compact('clientes','empleados','solicitudes'));
      }
      else {
        return redirect('/iniciar-sesion');
      }
    }
    public function verificar_tipo($id){
      $date = new DateTime();
      $data=request()->all();
      $user=User::findOrFail($id);
      $user->id_rol = $data['id_rol'];
      $user->emailV = $date->format('Y-m-d H:i');
      $user->save();
      return redirect('encargado/usuarios');
    }

    public function confirmar_cita($id){
      $cita = Cita::findOrFail($id);
      $cita->estado = 'Confirmada';
      $cita->save();
      return redirect('/encargado/citas');
    }
  }
