<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Input  as Input;
use App\Imagen;
use DateTime;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function actualizar(){
      if(Auth::check()){
        $user = Auth::user();
        return view('editar', compact('user'));
      }
      else {
        return redirect('/');
      }
    }

    public function update(){
      // return $user;
      $user = Auth::user();
      $data = request()->validate([
        'nombre' => 'required',
        'apPat' => 'required',
        'apMat' => 'required',
        'email' => 'required|email|unique::users,email,'.$user->id,
        'password' => ''
      ]);

      if($data['password'] != null){
        $data['password'] = bcrypt($data['password']);
      } else {
        unset($data['password']);
      }
      $user->update($data);
    }

    public function borrar(){
      return view('borrar');
    }

    public function delete(){
      // unlink(public_path('img/example.jpg'));
      // return redirect('/');
      $data = request()->all();
      $file = $data['imagen'];

      $aux = new DateTime();
      $date = $aux->format('Y-m-d-H-i-s');
      $fileName = $date.'_'.$file->getClientOriginalName();
      Imagen::create([
        'url' => 'img/'.$fileName
      ]);
      $currentImage = Imagen::where('url', 'img/'.$fileName)->first();
      $file->move('img',$fileName);
      echo '<img src="'.$currentImage->url.'" />';
    }
}
