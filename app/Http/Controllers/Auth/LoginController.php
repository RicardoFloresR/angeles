<?php

namespace App\Http\Controllers\Auth;
use Auth;
Use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
// use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
  public function login_view(){
    if(Auth::check()){
      return redirect(Auth::user()->rol->default_url);
    }
    else {
      return view('iniciar-sesion');
    }
  }

  public function register_view(){
    if(Auth::check()){
      return redirect(Auth::user()->rol->default_url);
    }
    else {
      return view('registrar');
    }
  }

  public function register(){
    $data = $this->validate(request(),[
      'nombre' => 'required|string',
      'apPat' => 'required|string',
      'apMat' => 'required|string',
      'email' => ['required', 'email', 'unique:users,email'],
      'password' => 'required|string',
    ]);
    User::create([
      'nombre' => $data['nombre'],
      'apPat' => $data['apPat'],
      'apMat' => $data['apMat'],
      'email' => $data['email'],
      'password' => bcrypt($data['password']),
      'id_rol' => 1,
      'emailV' => null
    ]);

    // return $data;
    return redirect('/');
  }

  public function login(){
    $credentials = $this->validate(request(), [
      'email' => 'email|required|string',
      'password' => 'required|string'
    ]);
    if(Auth::attempt($credentials)){
      $user = User::where('email', '=', $credentials['email'])->first();
      if($user->emailV == null){
        Auth::logout();
        // Retornar vista de NO VALIDADO
        return "no estas validado krnal, sorry";
      }
      return redirect(Auth::user()->rol->default_url);
    }
    else {
      return redirect('/iniciar-sesion');
    }

  }

  public function logout(){
    Auth()->logout();
    return redirect('/');
  }
}
