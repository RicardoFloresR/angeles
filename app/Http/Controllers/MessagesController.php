<?php

namespace App\Http\Controllers;

use App\Mail\RecoverPassword;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;

class MessagesController extends Controller
{
  public function recuperar_contraseña(){
    return view('recuperar-contraseña');
  }

  public function recover_password(Request $request){
    // $email = $this->validate(request(),[
    //   'email' => ['required', 'email', 'unique:users,email']
    // ]);
    $data = $this->validate(request(),[
      'test' => 'required|string',
    ]);
    Mail::to('example@example.com')->send(new RecoverPassword($data));
    return new RecoverPassword($data);
  }
}
