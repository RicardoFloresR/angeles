<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cita;
use App\Compra;
use App\Venta;
use App\Trabajo;

class ReceptionistController extends Controller
{
  public function recepcionista_citas(){
    $citas= Cita::all();
    return view('recepcionista/recepcionista-citas', compact('citas'));
  }
  public function recepcionista_compras(){
    $compras= Compra::all();
    return view('recepcionista/recepcionista-compras', compact('compras'));
  }
  public function recepcionista_ventas(){
    $ventas= Venta::all();
    return view('recepcionista/recepcionista-ventas', compact('ventas'));
  }
  public function confirmar_cita($id){
    $cita = Cita::findOrFail($id);
    $cita->estado = 'Confirmada';
    $cita->save();
    return redirect('/recepcionista/citas');
  }
}
