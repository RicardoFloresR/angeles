<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Trabajo;
use App\Material;
use App\Proveedor;
use App\User;
use App\Medida;
use App\Diagnostico;
use App\Plantilla;
use App\Zapato;
use Illuminate\Support\Facades\Validator;
use Auth;

class TechnicianController extends Controller
{
  public function tecnico_trabajos(){
    $trabajos = Trabajo::where('id_estado','!=',1)->get();
    return view('tecnico/tecnico-trabajos', compact('trabajos'));
  }
  public function tecnico_material(){
    $materiales = Material::all();
    $proveedores = Proveedor::all();
    return view('tecnico/tecnico-material', compact('materiales','proveedores'));
  }
  public function tecnico_clientes(){
    $diagnosticos = Diagnostico::all();
      return view('tecnico/tecnico-clientes', compact('diagnosticos'));
    }

  public function ver_expediente($id){
    $client= User::findOrFail($id);
    $diagnostic = Diagnostico::where('id_cliente','=',$client['id'])->first();
    $medid  = Medida::where('id_cliente','=',$client['id'])->first();
    return view('tecnico/cliente-expediente', compact('diagnostic','medid','client'));
  }
  public function cambiar_estado($id){
    $data=request()->all();
    $work=Trabajo::findOrFail($id);
    $work->id_estado = $data['id_estado'];
    $work->save();
    return redirect('tecnico/trabajos');
  }
  public function agregar_trabajo(){
    $medidas = Medida::all();
    $zapatos = Zapato::all();
    return view('tecnico/nuevo-trabajo', compact('medidas','zapatos'));
  }
  public function nuevo_trabajo(){
    $data = request()->validate([
      // Reglas de validacion
      'id_medida' => 'required|integer|exists:medida,id',
      'id_zapato' => 'required|integer|exists:zapato,id',
      'costo' => 'required|numeric',
      'precio' => 'required|numeric'
    ], [
      // Mensajes a enviar si no se cumple alguna regla
      'id_medida.integer' => 'Cliente: Cliente no valido, por favor seleccione uno correctamente.',
      'id_zapato.integer' => 'Zapato: Zapato no valido, por favor seleccione uno correctamente.',
      'costo.required' => 'Costo: Introduzca una cantidad.',
      'costo.numeric' => 'Costo: Cantidad no valida.',
      'precio.required' => 'Precio: Introduzca una cantidad.',
      'precio.numeric' => 'Precio: Cantidad no valida.'
    ]);
    $userID=Auth::user()->id;
    Trabajo::create([
      'id_tecnico' => $userID,
      'id_medida' => $data['id_medida'],
      'id_zapato' => $data['id_zapato'],
      'costo' => $data['costo'],
      'precio' => $data['precio'],
      'id_estado' => 4,
    ]);
    return redirect('/tecnico/trabajos');
  }
  public function eliminar_trabajo($id){
    $work=Trabajo::findOrFail($id);
    $work->delete();
    return redirect('/tecnico/trabajos');
  }
  public function agregar_proveedor(){
    $data = request()->validate([
      // Reglas de validacion
      'nombre' => 'required|string',
      'email' => 'required|string|exists:zapato,id',
      'numero_contacto' => 'required|numeric'
    ], [
      // Mensajes a enviar si no se cumple alguna regla
      'nombre.required' => 'Nombre: Introduzca un nombre para el producto.',
      'email.required' => 'Correo electronico: Introduzca un correo electronico.',
      'costo.required' => 'Costo: Introduzca un numero valido.',
      'costo.numeric' => 'Costo: Numero no valido.'
    ]);
    Proveedor::create([
      'nombre' => $data['nombre'],
      'email' => $data['email'],
      'numero_contacto' => $data['numero_contacto'],
    ]);
    return redirect('/tecnico/materiales');
  }
  public function editar_proveedor($id){
    $data=request()->validate([
      // Reglas de validacion
      'nombre' => 'required|string',
      'email' => 'required|string|exists:zapato,id',
      'numero_contacto' => 'required|numeric'
    ], [
      // Mensajes a enviar si no se cumple alguna regla
      'nombre.required' => 'Nombre: Introduzca un nombre para el proveedor.',
      'email.required' => 'Correo electronico: Introduzca un correo electronico.',
      'costo.required' => 'Costo: Introduzca un numero valido.',
      'costo.numeric' => 'Costo: Numero no valido.'
    ]);
    $proveedor=Proveedor::findOrFail($id);
    $proveedor->email = $data['email'];
    $proveedor->numero_contacto = $data['numero_contacto'];
    $proveedor->save();
    return redirect('tecnico/materiales');
  }
  public function cambiar_stock($id){
    $data= request()->validate([
      // Reglas de validacion
      'stock' => 'required|integer'
    ], [
      // Mensajes a enviar si no se cumple alguna regla
      'stock.required' => 'Stock: Introduzca una cantidad.',
      'stock.numeric' => 'Stock: Cantidad no valida.',
    ]);
    $material= Material::findOrFail($id);
    $material->stock=$data['stock'];
    $material->save();
    return redirect('tecnico/materiales');
    }
    public function nuevo_material(){
      $proveedores = Proveedor::all();
      return view('nuevo-material', compact('proveedores'));
    }
    public function agregar_material(){
      $data = request()->validate([
        // Reglas de validacion
        'nombre.required' => 'Nombre: Introduzca un nombre para el material.',
        'id_proveedor' => 'required|integer|exists:proveedor,id',
        'stock' => 'required|integer',
        'unidad_de_medida' => 'required|string'
      ], [
        // Mensajes a enviar si no se cumple alguna regla
        'nombre.required' => 'Nombre: Introduzca un nombre para el producto.',
        'id_proveedor.integer' => 'Proveedor: Proveedor no valido, por favor seleccione uno correctamente.',
        'stock.required' => 'Stock: Introduzca una cantidad.',
        'stock.numeric' => 'Stock: Cantidad no valida.',
        'unidad_de_medida.require' => 'Unidad de medida: Introduzca una unidad valida.'
      ]);
      Material::create([
        'nombre' => $data['nombre'],
        'id_proveedor' => $data['id_proveedor'],
        'stock' => $data['stock'],
        'unidad_de_medida' => $data['unidad_de_medida'],
      ]);
      return redirect('/tecnico/materiales');
    }
    public function nuevo_expediente(){
      $diagnosticos = Diagnostico::all();
      foreach ($diagnosticos as $diagnostico) {
        $data[]=$diagnostico->id_cliente;
      }
      $clientes = User::where('id_rol',1)->whereNotIn('id',$data)->whereNotNull('emailV')->get();
      return view('tecnico/agregar-expediente', compact('clientes'));
    }
    public function agregar_expediente(){
      $data = request()->validate([
        'diagnostico' => 'required',
        'cliente_id' => 'required|integer|exists:users,id',
        'LDpieI' => 'required|integer',
        'ADTalonI' => 'required|integer',
        'ADArcoI' => 'required|integer',
        'MDBotonI' => 'required|integer',
        'LDpieD' => 'required|integer',
        'ADTalonD' => 'required|integer',
        'ADArcoD' => 'required|integer',
        'MDBotonD' => 'required|integer'
      ], [
        // Mensajes a enviar si no se cumple alguna regla
        'diagnostico.required' => 'Diagnostico: Introduzca un diagnostico para el expediente.',
        'cliente_id.integer' => 'Cliente: Cliente no valido, por favor seleccione uno correctamente.',
        'LDpieI.required' => 'Largo de pie: Introduzca una cantidad.',
        'LDpieI.numeric' => 'Largo de pie: Cantidad no valida.',
        'ADTalonI.required' => 'Ancho de Talon: Introduzca una cantidad.',
        'ADTalonI.numeric' => 'Ancho de talon: Cantidad no valida.',
        'ADArcoI.required' => 'Ancho de arco: Introduzca una cantidad.',
        'ADArcoI.numeric' => 'Ancho de arco: Cantidad no valida.',
        'MDBotonI.required' => 'Medida de boton: Introduzca una cantidad.',
        'MDBotonI.numeric' => 'Medida de boton: Cantidad no valida.',
        'LDpieD.required' => 'Largo de pie: Introduzca una cantidad.',
        'LDpieD.numeric' => 'Largo de pie: Cantidad no valida.',
        'ADTalonD.required' => 'Ancho de Talon: Introduzca una cantidad.',
        'ADTalonD.numeric' => 'Ancho de talon: Cantidad no valida.',
        'ADArcoD.required' => 'Ancho de arco: Introduzca una cantidad.',
        'ADArcoD.numeric' => 'Ancho de arco: Cantidad no valida.',
        'MDBotonD.required' => 'Medida de boton: Introduzca una cantidad.',
        'MDBotonD.numeric' => 'Medida de boton: Cantidad no valida.'
      ]);
      Diagnostico::create([
        'diagnostico' => $data['diagnostico'],
        'id_cliente' => $data['cliente_id'],
      ]);
      Medida::create([
        'LDpieI' => $data['LDpieI'],
        'ADTalonI' => $data['ADTalonI'],
        'ADArcoI' => $data['ADArcoI'],
        'MDBotonI' => $data['MDBotonI'],
        'LDpieD' => $data['LDpieD'],
        'ADTalonD' => $data['ADTalonD'],
        'ADArcoD' => $data['ADArcoD'],
        'MDBotonD' => $data['MDBotonD'],
        'id_cliente' => $data['cliente_id'],
      ]);
        return redirect('/tecnico/clientes');
    }

}
