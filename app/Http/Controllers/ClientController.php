<?php

namespace App\Http\Controllers;
use App\User;
use DateTime;
use App\Producto;

use Illuminate\Http\Request;

class ClientController extends Controller
{
    public function welcome(){
      return view('welcome');
    }
    public function creando_usuario(){
      $data = request()->all();
      User::create([
        'nombre' => $data['nombre'],
        'apPat' => $data['apPat'],
        'apMat' => $data['apMat'],
        'email' => $data['email'],
        'password' => $data['password'],
        'emailV' => null,
        'id_rol' => 1,
      ]);
      return redirect('/');
    }
    public function ver_catalogo(){
      $productos = Producto::all();
      return view('/cliente-catalogo', compact('productos'));
    }

}
