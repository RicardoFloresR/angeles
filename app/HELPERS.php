
<?php
function login_logout(){
  if(Auth::check()){
    echo '<a class="btn btn-info h-50 flex items-center" href="'.url('/editar-perfil').'">Ver perfil</a><a class="btn btn-info mh3 h-50 flex items-center" href="'.url('/logout').'">Cerrar sesión</a>';
  }
  else{
    echo '<a class="btn btn-info mh3 h-50 flex items-center" href="'.url('/iniciar-sesion').'" >Iniciar sesión</a>';
  }
}

function welcome_user(){
  if(Auth::check()){
    echo '¡Bienvenido! '.Auth::user()->rol->nombre.': '.Auth::user()->nombre;
  }
  else {
    echo '¡Bienvenido! Puedes registrarte dando click <a href="'.url('registrarse').'">aquí</a> para recibir atención personalizada';
  }
}
