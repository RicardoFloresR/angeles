<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Compra extends Model
{
    protected $table = "compra";

    protected function proveedor(){
      return $this->belongsTo(Proveedor::class, 'id_proveedor');
    }
    protected function material(){
      return $this->belongsTo(Material::class, 'id_material');
    }
}
