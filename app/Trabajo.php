<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trabajo extends Model
{
    protected $table = "trabajo";

    protected $fillable = [
        'id_tecnico', 'id_medida', 'id_zapato', 'costo', 'precio', 'id_estado'
    ];
    protected function estado(){
      return $this->belongsTo(Estado::class, 'id_estado');
    }
    protected function zapato(){
      return $this->belongsTo(Zapato::class, 'id_zapato');
    }
    protected function user(){
      return $this->belongsTo(User::class, 'id_tecnico');
    }
    protected function medida(){
      return $this->belongsTo(User::class, 'id_medida');
    }
}
