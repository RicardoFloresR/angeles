<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Venta extends Model
{
    protected $table = "venta";

    protected function usuario(){
      return $this->belongsTo(User::class, 'id_cliente');
    }
    protected function trabajo(){
      return $this->belongsTo(Trabajo::class, 'id_trabajo');
    }
}
