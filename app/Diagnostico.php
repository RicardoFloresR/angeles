<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Diagnostico extends Model
{
    protected $table = "diagnostico";
    protected function user(){
      return $this->belongsTo(User::class, 'id_cliente');
    }
    protected $fillable = [
        'id_cliente', 'diagnostico', 'id_cita'
    ];
}
