<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Material extends Model
{
    protected $table = "material";

    protected function proveedor(){
      return $this->belongsTo(Proveedor::class, 'id_proveedor');
    }
    protected $fillable = [
        'id_proveedor','nombre', 'stock','unidad_de_medida'
    ];
}
