@extends('layouts/base')
@section('content')

<div class="w3-container w3-content w3-padding-64" style="max-width:1000px" id="pd">
    <h2>Políticas de Envíos</h2><br>

<a>-Los envíos se hacen a toda la República Mexicana mediante servicio de paqueteria convencional.</a><br><br>

<a>-ENTREGA EN DOMICILIO.- Para realizar nuestros envíos utilizamos paquetería FEDEX Y UPS  para productos que no excedan los 80 kg, en esta </a><br>
<a> modalidad el cliente podrá escoger el modo de envió, de 2 a 3 días hábiles a partir de la fecha de confirmación del pago del producto.</a><br><br>

<a>- ENTREGA EN SUCURSAL.- (Valido únicamente para pagos realizados con transferencia bancaria) En el caso que el cliente opte por la </a><br>
<a>recolección del producto en alguna de nuestras sucursales, la cual no tiene costo para el cliente, esta se realizara al siguiente día </a><br>
<a> hábil de haber realizado su compra después de las 15:00 hrs. para recoger el paquete solo deberá de llevar lleno el formato de producto></a><br>
<a>recibido junto con una identificación oficial.</a><br><br>


<a>- Los tiempos de entrega son especificados en la orden según el tiempo de entrega seleccionado por el cliente al realizar la compra,</a><br>
<a> recordando que los días hábiles se manejan de lunes a viernes sin incluir días feriados y partiendo del día posterior a la compra del producto.</a><br><br>

<a>- Todos los productos por la naturaleza de los mismos cuentan con un periodo de 5 días naturales para poder pedir un cambio por defecto de </a><br>
<a> fabrica a partir de la entrega marcada por la paquetería.</a><br>
</div>
@endsection
