@extends('layouts/base')
@section('head')
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
@endsection
@section('content')
<div class="row nav-bar">
  <a class="w-20" href="{{url('/cliente/home')}}"><div class="nav-button w-100 br3 br--left">Home</div></a>
  <a class="w-20" href="{{url('/cliente/catalogo')}}"><div class="nav-button w-100">Catálogo/Tienda</div></a>
  <a class="w-20" href="{{url('/cliente/ofertas')}}"><div class="nav-button w-100">Ofertas</div></a>
  <a class="w-20" href="{{url('/cliente/citas')}}"><div class="nav-button w-100">Citas</div></a>
  <a class="w-20" href="{{url('/cliente/contactanos')}}"><div class="nav-button-active w-100 br3 br--right">Contactanos</div></a>
</div>
<div class="w3-container w3-content w3-padding-64" style="max-width:800px" id="contact">
    <h2 class="w3-wide w3-center">Contactanos</h2>
    <p class="w3-opacity w3-center"><i>Envianos un Mensaje</i></p>
    <div class="w3-row w3-padding-32">
      <div class="w3-col m6 w3-large w3-margin-bottom">
        <p><b>Información de Contacto</b></p>
        <i class="fa fa-phone" style="width:30px"></i> Teléfono: (52) 2387472 ext.20<br>
        <i class="fa fa-envelope" style="width:30px"> </i> Email: angeles@ortopedia.com.mx <br>
        <i class="fa fa-map-marker" style="width:30px"></i> <a>Dirección: Miguel Hidalgo #34,</a><br>
        <a>Col. San Juan , Del. Cuahutemoc,</a><br>
        <a> 56624, CDMX</a><br><br>

        <i style="width:30px"></i> Horarios de atención de Lunes <br>
         a Viernes de 9:00 a 18:00 hrs.<br>

      </div>
      <div class="w3-col m6">
        <form action="/action_page.php" target="_blank">
          <div class="w3-row-padding" style="margin:0 -16px 8px -16px">
            <div class="w3-half">
              <label for="nombre">Nombre</label>
              <input class="w3-input w3-border" type="text" placeholder="" required name="nombre">
            </div>
            <div class="w3-half">
              <label for="correo">Correo Electrónico</label>
              <input class="w3-input w3-border" type="email" placeholder="" required name="correo">
            </div>
          </div>
          <label for="asunto">Asunto</label>
          <input class="w3-input w3-border" type="text" placeholder="" required name="asunto">
          <label for="apMat">¿En qué te podemos ayudar?</label>
          <input class="w3-input w3-border" type="text" placeholder="" required name="ayuda"><br>
          <input class="btn btn-primary w3-right" type="submit" name="" value="Enviar">
        </form>
      </div>
    </div>
  </div>
</div>
@endsection
