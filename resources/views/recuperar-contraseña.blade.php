@extends('layouts/base')
@section('content')
<div class="d-flex jc-c w-100 row">
  <div class="w3-container w3-content w3-padding-64 w-40" style="max-width:1000px">
    <h2 class="w3-wide w3-center">Recuperar Contraseña</h2><br>
      <div class="w-center">
  <form class="" action="{{url('/recover_password')}}" method="post">
  {!! csrf_field() !!}
  <div class="form-group" align="center">
  <input class="" type="text" name="test" value="">
  <input class="btn btn-primary" type="submit" name="" value="Enviar">
</div>
</form>
</div>
</div>
</div>
@endsection
