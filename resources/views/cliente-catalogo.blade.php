@extends('layouts/base')
@section('content')
<div class="row nav-bar">
  <a class="w-20" href="{{url('/cliente/home')}}"><div class="nav-button w-100 br3 br--left">Home</div></a>
  <a class="w-20" href="{{url('/cliente/catalogo')}}"><div class="nav-button-active w-100">Catálogo/Tienda</div></a>
  <a class="w-20" href="{{url('/cliente/ofertas')}}"><div class="nav-button w-100">Ofertas</div></a>
  <a class="w-20" href="{{url('/cliente/citas')}}"><div class="nav-button w-100">Citas</div></a>
  <a class="w-20" href="{{url('/cliente/contactanos')}}"><div class="nav-button w-100 br3 br--right">Contactanos</div></a>
</div>
<style media="screen">
</style>
</script>
<div class="container text-center ">
  <div id="productos" class="flex-container">
    @foreach ($productos as $producto)
      <div class="producto white-panel w-40">
          <h3>{{$producto->nombre}}</h3>
          <img src="{{$producto->imagen->url}}" width="200">
          <div class="producto-info panel">
              <p>Precio: ${{number_format($producto->precio)}}</p>
              <p>
                <a href="#" class="btn btn-danger"> <i class="fa fa-cart-plus"></i> Agregar al carrito</a>
              </p>
          </div>
      </div>
      @endforeach
  </div>
</div>
@endsection
@endsection
