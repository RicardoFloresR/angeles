@extends('layouts/base')
@section('head')
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
@endsection
@section('content')
<div class="row nav-bar">
  <a class="w-25" href="{{url('/')}}"><div class="nav-button w-100 br3 br--left">Home</div></a>
  <a class="w-25" href="{{url('/Catalogo')}}"><div class="nav-button w-100">Catálogo/Tienda</div></a>
  <a class="w-25" href="{{url('/Ofertas')}}"><div class="nav-button w-100">Ofertas</div></a>
  <a class="w-25" href="{{url('/Contactanos')}}"><div class="nav-button w-100 br3 br--right">Contactanos</div></a>
</div>
  <div class="w3-container w3-content w3-padding-64" style="max-width:800px">
    <div class="w3-panel w3-pale-blue w3-leftbar w3-rightbar w3-border-blue">
      <br><br><br><br>
      <h3>Tu cuenta esta en espera de validación!!</h3>
      <p>Se te enviará un correo en cuanto sea verificada por el encargado</p>
      <br><br><br><br>
      <div class="w-100" align="right">
        <a href="{{url('/registrarse')}}" class="btn btn-primary">Regresar</a>
        <a href="{{url('/')}}" class="btn btn-primary">Aceptar</a>
      </div>
  </div>

    </div>



@endsection
