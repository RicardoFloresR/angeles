@extends('layouts/base')
@section('content')

<div class="w3-container w3-content w3-padding-64" style="max-width:1000px" id="pd">
    <h2>Políticas de Devolución</h2><br>

<a>Nuestra política de devolución es muy sencilla; el cliente podrá devolver cualquier artículo comprado bajo las siguientes condiciones:</a><br><br>

<a>1.  Si el artículo presenta <b>defectos de fabricación, descompostura o ruptura que no hayan sido provocadas por mal uso y/o maltrato del producto.</b></a><br>
<a>2. <b>Si existe equivocación en el artículo enviado por parte de la empresa.</b></a><br><br>

<a>Para hacer válida la garantía el cliente deberá:</a><br><br>

<a>1. Reportarlo durante los primeros 5 días hábiles, posteriores a su entrega, presentando su nota de factura al siguiente teléfono:(52) 2387472 ext.20 </a><br>
<a>o al correo electrónico angeles@ortopedia.com.mx en un horario de 9:00 a 18:00 hrs. de lunes a viernes, por ningún motivo</a><br>
<a>se harála devolución fuera del plazo estipulado.</a><br>
<a>2. <b>Es INDISPENSABLE</b> conservar la envoltura original , así mismo el artículo NO deberá presentar muestras de maltrato.</a><br>
<a>3. El cliente deberá enviar un correo <b>integrando fotografías del defecto del producto, número de venta, motivo por el cual se tendría que hacer válida la garantía</b></a><br><br>

<a><b>POR HIGIENE NO HAY CAMBIO NI DEVOLUCIÓN </b>en los siguientes productos:</a><br><br>
<a>- CALCETINES, MEDIAS ELÁSTICAS O DE COMPRESIÓN Y BOTAS.</a><br>
<a>- ARTÍCULOS PARA EL PIE (cremas, lociones, plantillas, accesorios para dedos, etc.)</a><br><br>

<a><b>GARANTÍA LIMITADA:</b> Todos los productos cuentan con una garantía por defecto de fabricación de 5 días directamente en la pagina web ( solo aplica para la</a><br>
<a> venta en línea, quedando deslindadas cualquier sucursal u otro punto de venta), después de este plazo la garantía la determina directamente el fabricante </a><br><br>

<b>LA GARANTÍA QUEDA ANULADA:</b><br><br>

<a>- Si fue manipulado por terceros no autorizados.</a><br>
<a>- Por uso inadecuado del producto.</a><br>
  </div>
@endsection
