@extends('layouts/base')
@section('content')
<div class="row nav-bar">
  <a class="w-third" href="{{url('/recepcionista/citas')}}"><div class="nav-button w-100 br3 br--left">Citas</div></a>
  <a class="w-third" href="{{url('/recepcionista/compras')}}"><div class="nav-button-active w-100">Compras</div></a>
  <a class="w-third" href="{{url('/recepcionista/ventas')}}"><div class="nav-button w-100 br3 br--right">Ventas</div></a>
</div>
  <div class="row">
    <div class="w-100">
      <div id="listado" class="">
        <div class="w-100 bg-light br3 mv3">
          <a href="#" class="btn btn-primary">+Agregar nueva compra</a>
        </div>
        <div class="w-100 table-responsive">
          <table class="table table-striped">
            <thead>
              <tr>
                <th>Codigo</th>
                <th>Proveedor</th>
                <th>Material</th>
                <th>Dia</th>
                <th>Hora</th>
                <th>Monto</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($compras as $compra)
              <tr>
                <th>{{$compra->id}}</th>
                <td>{{$compra->proveedor->nombre}}</td>
                <td>{{$compra->material->nombre}}</td>
                <td>{{\Carbon\Carbon::parse($compra->horaDeCompra)->format('d-m-Y')}}</td>
                <td>{{\Carbon\Carbon::parse($compra->horaDeCompra)->format('h:i')}}</td>
                <td>$ {{$compra->monto}}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection
