@extends('layouts/base')
@section('content')
<div class="row nav-bar">
  <a class="w-third" href="{{url('/recepcionista/citas')}}"><div class="nav-button w-100 br3 br--left">Citas</div></a>
  <a class="w-third" href="{{url('/recepcionista/compras')}}"><div class="nav-button w-100">Compras</div></a>
  <a class="w-third" href="{{url('/recepcionista/ventas')}}"><div class="nav-button-active w-100 br3 br--right">Ventas</div></a>
</div>
  <div class="row">
    <div class="w-100">
      <div id="listado" class="">
        <div class="w-100 bg-light br3 mv3">
          <a href="#" class="btn btn-primary">+Agregar nueva venta</a>
        </div>
        <div class="w-100 table-responsive">
          <table class="table table-striped">
            <thead>
              <tr>
                <th>Codigo</th>
                <th>Nombre de cliente</th>
                <th>Dia</th>
                <th>Hora</th>
                <th>Monto</th>
                <th>Acciones</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($ventas as $venta)
              <tr>
                <th>{{$venta->id}}</th>
                <td>{{$venta->usuario->nombre}} {{$venta->usuario->apPat}} {{$venta->usuario->apMat}}</td>
                <td>{{\Carbon\Carbon::parse($venta->Hora_Registro)->format('d-m-Y')}}</td>
                <td>{{\Carbon\Carbon::parse($venta->Hora_Registro)->format('h:i')}}</td>
                <td>$ {{$venta->trabajo->precio}}</td>
                <td>
                  <div class="w-100">
                    <button class="btn btn-danger" type="button" name="button">Eliminar</button>
                    <button class="btn btn-primary" type="button" name="button" href="#">Editar</button>
                  </div>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection
