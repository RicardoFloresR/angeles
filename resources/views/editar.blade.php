@extends('layouts/base')
@section('content')
<div class="d-flex jc-c w-100 row">
  <span class="w-100 f2" align="center">Editar perfil</span>
  <div class="w-50 f4" align="center">
  <form class="" action="{{url('/register')}}" method="post">
    {!! csrf_field() !!}
    <div class="form-group">
      <label for="nombre">Nombre</label>
      <input name="nombre" type="text" class="form-control tc" placeholder="" value="{{old('nombre', $user->nombre)}}" required>
      {!! $errors->first('nombre', '<span class="login-error">Este campo es obligatorio</span>') !!}
    </div>
    <div class="form-group">
      <label for="apPat">Apellido paterno</label>
      <input name="apPat" type="text" class="form-control tc"placeholder="Introduzca su apellido paterno" value="{{old('apPat', $user->apPat) }}" required>
      {!! $errors->first('apPat', '<span class="login-error">Este campo es obligatorio</span>') !!}
    </div>
    <div class="form-group">
      <label for="apMat">Apellido materno</label>
      <input name="apMat" type="text" class="form-control tc"placeholder="Introduzca su apellido materno" value="{{old('apMat', $user->apMat) }}" required>
      {!! $errors->first('apMat', '<span class="login-error">Este campo es obligatorio</span>') !!}
    </div>
    <div class="form-group">
      <label for="exampleInputEmail1">Correo electrónico</label>
      <input name="email" type="email" class="form-control tc" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Introduzca su correo electrónico" value="{{old('email', $user->email) }}" required>
      {!! $errors->first('email', '<span class="login-error">El correo brindado ya está en uso</span>') !!}
    </div>
    <div class="form-group">
      <label for="exampleInputPassword1">Contraseña</label>
      <input name="password" type="password" class="form-control tc" id="exampleInputPassword1" placeholder="Introduzca una contraseña nueva (opcional)" required>
    </div>
    <div class="form'group">
      <input class="btn btn-primary" type="submit" name="" value="Actualizar información">
    </div>
  </form>
  <!-- <div class="form-text text-muted mt4">
    ¿No estás registrado? Regístrate dando click <a href="{{url('/registrarse')}}">aquí</a>
  </div> -->
  </div>
  <div class="w-80 flex justify-end">
    <a href="{{url()->previous()}}" class="btn btn-danger">Cancelar</a>
  </div>
</div>
@endsection
