@extends('layouts/base')
@section('content')
<form class="" action="{{url('/delete')}}" method="post" enctype="multipart/form-data">
  {!! csrf_field() !!}
  <input type="file" name="imagen" id="imagen" value="" accept="image/*">
  <input class="btn btn-primary" type="submit" name="" value="subir">
</form>
@endsection
