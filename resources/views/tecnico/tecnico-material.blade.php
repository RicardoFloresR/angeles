@extends('layouts/base')
@section('content')
<div class="row nav-bar">
  <a class="w-third" href="{{url('/tecnico/trabajos')}}"><div class="nav-button w-100 br3 br--left">Trabajos</div></a>
  <a class="w-third" href="{{url('/tecnico/materiales')}}"><div class="nav-button-active w-100">Materiales</div></a>
  <a class="w-third" href="{{url('/tecnico/clientes')}}"><div class="nav-button w-100">Clientes</div></a>
</div>
<div class="row">
  <div class="w-20 br2">
    <button class="tablinks tab-button w-100" onclick="openContent(event, 'listado');">Listado</button>
    <button class="tablinks tab-button w-100" onclick="openContent(event, 'proveedores');">Proveedores</button>
  </div>
  <div class="w-80">
    <div id="listado" class="tabcontent">
      <div class="w-100 bg-light br3 mv3">
        <a href="{{url('tecnico/materiales/nuevo_material')}}" class="btn btn-primary">+Agregar material</a>
      </div>
      <div class="w-100 table-responsive">
        <table class="table table-striped">
          <thead>
            <tr>
              <th>#</th>
              <th>Nombre</th>
              <th>Proveedor</th>
              <th>Stock</th>
              <th>Unidad de medida</th>
              <th>Opciones</th>
            </tr>
          </thead>
          <tbody>
            @foreach($materiales as $material)
            <tr>
              <th scope="row">{{$material->id}}</th>
              <td>{{$material->nombre}}</td>
              <td>{{$material->proveedor->nombre}}</td>
              <td>{{$material->stock}}</td>
              <td>{{$material->unidad_de_medida}}</td>
              <td>
                <a class="btn btn-primary" href="{{url('tecnico/materiales/remesa/'.$material->id)}}">Solicitar remesa</a>
                <button class="btn btn-warning" type="button"name="button" data-toggle="modal" data-target="{{'#modal3'.$material->id}}">Cambiar stock</button>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
    <div id="proveedores" class="tabcontent">
      <div class="w-100 bg-light br3 mv3">
      <button class="btn btn-primary" type="button"name="button" data-toggle="modal" data-target="{{'#modal'}}">+Agregar Proveedor</button>
      </div>
      <div class="w-100 table-responsive">
        <table class="table table-striped">
          <thead>
            <tr>
              <th>Codigo</th>
              <th>Nombre</th>
              <th>E-mail de contacto</th>
              <th>Numero de contacto</th>
            </tr>
          </thead>
          <tbody>
            @foreach($proveedores as $proveedor)
            <tr>
              <th>{{$proveedor->id}}</th>
              <td>{{$proveedor->nombre}}</td>
              <td>{{$proveedor->email}}</td>
              <td>{{$proveedor->numero_contacto}}</td>
              <td>
                <div class="w-100">
                  <button class="btn btn-primary" type="button"name="button" data-toggle="modal" data-target="{{'#modal2'.$proveedor->id}}">Editar</button>
                </div>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@section('modal')
<div class="modal fade" id="{{'modal'}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title bg-1D5261"  id="exampleModalLongTitle">Nuevo proveedor:</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body flex justify-center">
        <form class="" action="{{url('/tecnico/materiales/nuevo_proveedor')}}" method="post">
          {!! csrf_field() !!}
          <div class="form-group">
            <label for="">Nombre</label><br>
            <input type="text" name="nombre" value="">
          </div>
          <div class="form-group">
            <label for="">Correo electronico</label><br>
            <input type="text" name="email" value="">
          </div>
          <div class="form-group">
            <label for="">Telefono de contacto</label> <br>
            <input type="text" name="numero_contacto" value="">
          </div>
          <div class="form-group">
            <input class="btn btn-primary" type="submit" name="" value="Guardar">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
          </div>
        </form>
        @if ($errors->any())
        <div class="alert alert-danger w-60 mh3">
          <h2>Errores en los datos introducidos:</h2>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
      </div>
    </div>
  </div>
</div>
@foreach($proveedores as $proveedor)
<div class="modal fade" id="{{'modal2'.$proveedor->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title bg-1D5261"  id="exampleModalLongTitle">Editar proveedor: {{$proveedor->nombre}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body flex justify-center">
        <form class="" action="{{url('/tecnico/materiales/editar_proveedor/'.$proveedor->id)}}" method="post">
          {!! csrf_field() !!}
          <div class="form-group">
            <label for="">Correo electronico</label><br>
            <input type="text" name="email" value="{{$proveedor->email}}">
          </div>
          <div class="form-group">
            <label for="">Telefono de contacto</label> <br>
            <input type="text" name="numero_contacto" value="{{$proveedor->numero_contacto}}">
          </div>
          <div class="form-group">
            <input class="btn btn-primary" type="submit" name="" value="Guardar">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
          </div>
        </form>
        @if ($errors->any())
        <div class="alert alert-danger w-60 mh3">
          <h2>Errores en los datos introducidos:</h2>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
      </div>
    </div>
  </div>
</div>
@endforeach
@foreach($materiales as $material)
<div class="modal fade" id="{{'modal3'.$material->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title bg-1D5261"  id="exampleModalLongTitle">Editar material: {{$material->nombre}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body flex justify-center">
        <form class="" action="{{url('/tecnico/materiales/cambiar_stock/'.$material->id)}}" method="post">
          {!! csrf_field() !!}
          <div class="form-group">
            <label for="">Stock</label> <br>
            <input type="number" name="stock" value="">
          </div>
          <div class="form-group">
            <input class="btn btn-primary" type="submit" name="" value="Guardar">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endforeach
@endsection
@endsection
