@extends('layouts/base')
@section('content')
<div class="row nav-bar">
  <a class="w-third" href="{{url('/tecnico/trabajos')}}"><div class="nav-button w-100 br3 br--left">Trabajos</div></a>
  <a class="w-third" href="{{url('/tecnico/materiales')}}"><div class="nav-button w-100">Materiales</div></a>
  <a class="w-third" href="{{url('/tecnico/clientes')}}"><div class="nav-button w-100 br3 br--right">Clientes</div></a>
</div>
<div class="row">
  <div class="w-100 flex">
    <form class="w-40" action="{{url('/tecnico/clientes/agregar_expediente')}}" method="post">
      <h3>Nuevo expediente</h3>
      {!! csrf_field() !!}
      <div class="form-group">
        <label for="">Cliente:</label><br>
        <select class="form-control" name="cliente_id">
          @foreach ($clientes as $cliente)
            <option value="{{$cliente->id}}">{{$cliente->nombre}} {{$cliente->apPat}} {{$cliente->apMat}}</option>
          @endforeach
        </select>
      </div>
      <div class="form-group w-60">
        <label for="diagnostico">Diagnostico</label><br>
        <textarea name="diagnostico" rows="3" cols="80"></textarea>
      </div>
      <div class="">
          <h3>Medidas</h3>
      </div>
      <div class="form-group w-60">
      <table class="table table-striped">
        <thead>
          <tr>
            <!--  -->
            <th>Pie</th>
            <th>Largo de Pie</th>
            <th>Ancho de talon</th>
            <th>Ancho de Arco</th>
            <th>Medida de Boton</th>
          </tr>
        </thead>
        <tbody>
        <tr>
          <td>Izquierdo</td>
          <td><input type="number" name="LDpieI" value=""> mm</td>
          <td><input type="number" name="ADTalonI" value=""> mm</td>
          <td><input type="number" name="ADArcoI" value=""> mm</td>
          <td><input type="number" name="MDBotonI" value=""> mm</td>
        </tr>
        <tr>
          <td>Derecho</td>
          <td><input type="number" name="LDpieD" value=""> mm</td>
          <td><input type="number" name="ADTalonD" value=""> mm</td>
          <td><input type="number" name="ADArcoD" value=""> mm</td>
          <td><input type="number" name="MDBotonD" value=""> mm</td>
        </tr>
        </tbody>
      </table>
    </div>
      <div class="form-group">
        <input class="btn btn-primary" type="submit"  value="Guardar">
        <a class="btn btn-danger" href="{{url('/tecnico/clientes')}}">Cancelar</a>
      </div>
  </form>
    @if ($errors->any())
    <div class="alert alert-danger w-20 mh3">
      <h2>Errores en los datos introducidos:</h2>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
  </div>
</div>
@endsection
