@extends('layouts/base')
@section('content')
<div class="row nav-bar">
  <a class="w-third" href="{{url('/tecnico/trabajos')}}"><div class="nav-button-active w-100 br3 br--left">Trabajos</div></a>
  <a class="w-third" href="{{url('/tecnico/materiales')}}"><div class="nav-button w-100">Materiales</div></a>
  <a class="w-third" href="{{url('/tecnico/clientes')}}"><div class="nav-button w-100">Clientes</div></a>
</div>
<div class="row">
  <div class="w-100">
    <div id="trabajos" class="">
      <div class="w-100 bg-light br3 mv3">
        <a href="{{url('/tecnico/trabajos/agregar_trabajo')}}" class="btn btn-primary">+Agregar trabajo</a>
      </div>
      <div class="w-100 table-responsive">
        <table class="table table-striped">
          <thead>
            <tr>
              <!--  -->
              <th>#</th>
              <th>Tecnico</th>
              <th>Modelo</th>
              <th>Color</th>
              <th>Material</th>
              <th>Estado</th>
              <th>Costo</th>
              <th>Precio</th>
            </tr>
          </thead>
          <tbody>
            @foreach($trabajos as $trabajo)
            <tr>
              <th scope="row">{{$trabajo->id}}</th>
              <td>{{$trabajo->user->nombre}} {{$trabajo->user->apPat}} {{$trabajo->user->apMat}}</td>
              <td>{{$trabajo->zapato->modelo->nombre}}</td>
              <td>{{$trabajo->zapato->color}}</td>
              <td>{{$trabajo->zapato->material}}</td>
              <td>{{$trabajo->estado->estado}}</td>
              <td>{{$trabajo->costo}}</td>
              <td>{{$trabajo->precio}}</td>
              <td>
                  <button class="btn btn-primary" type="button"name="button" data-toggle="modal" data-target="{{'#modal'.$trabajo->id}}">Cambiar estado</button>
                  <button class="btn btn-danger" type="button"name="button" data-toggle="modal" data-target="{{'#modal2'.$trabajo->id}}">Eliminar</button>

              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@section('modal')
@foreach($trabajos as $trabajo)
<div class="modal fade" id="{{'modal'.$trabajo->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title bg-1D5261"  id="exampleModalLongTitle">Estado de trabajo:</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body flex justify-center">
          <form class="" action="{{url('/tecnico/trabajos/cambiar_estado/'.$trabajo->id)}}" method="post">
          {!! csrf_field() !!}
            <div class="form-group">
              <label for="id_estado">Estado:</label> <br>
                <select class="form-control" name="id_estado" >
                  <option value="1">Terminado</option>
                  <option value="2">En Proceso </option>
                  <option value="3">En espera de material</option>
                  <option value="4">Iniciado</option>
                </select>
            </div>
            <div class="form-group">
              <input class="btn btn-primary" type="submit" name="" value="Guardar">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
            </div>
          </form>
          @if ($errors->any())
          <div class="alert alert-danger w-60 mh3">
            <h2>Errores en los datos introducidos:</h2>
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
          @endif
      </div>
    </div>
  </div>
</div>
@endforeach
@foreach($trabajos as $trabajo)
<div class="modal fade" id="{{'modal2'.$trabajo->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title bg-1D5261"  id="exampleModalLongTitle">Eliminar trabajo:</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body flex justify-center">
          <form class="" action="{{url('/tecnico/trabajos/eliminar/'.$trabajo->id)}}" method="post">
          {!! csrf_field() !!}
            <div class="form-group">
                <h4>Esta seguro que desea eliminar este trabajo?</h4>
            </div>
            <div class="form-group">
              <input class="btn btn-primary" type="submit" name="" value="Aceptar">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
            </div>
          </form>
      </div>
    </div>
  </div>
</div>
@endforeach
@endsection
@endsection
