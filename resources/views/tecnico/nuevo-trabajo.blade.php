@extends('layouts/base')
@section('content')
<div class="row nav-bar">
  <a class="w-third" href="{{url('/tecnico/trabajos')}}"><div class="nav-button-active w-100 br3 br--left">Trabajos</div></a>
  <a class="w-third" href="{{url('/tecnico/materiales')}}"><div class="nav-button w-100">Materiales</div></a>
  <a class="w-third" href="{{url('/tecnico/clientes')}}"><div class="nav-button w-100 br3 br--right">Clientes</div></a>
</div>
<div class="row">
  <div class="w-100 flex">
    <form class="w-40" action="{{url('/tecnico/trabajos/nuevo_trabajo')}}" method="post">
      <h3>Nuevo trabajo</h3>
      {!! csrf_field() !!}
      <div class="form-group">
        <label for="">Cliente:</label><br>
        <select class="form-control" name="id_medida">
          @foreach ($medidas as $medida)
            <option value="{{$medida->id}}">{{$medida->user->nombre}} {{$medida->user->apPat}} {{$medida->user->apMat}}</option>
          @endforeach
        </select>
      </div>
      <div class="form-group">
        <label for="">Modelo de zapato:</label><br>
        <select class="form-control" name="id_zapato">
          @foreach ($zapatos as $zapato)
            <option value="{{$zapato->id}}">{{$zapato->modelo->nombre}} {{$zapato->color}} {{$zapato->material}}</option>
          @endforeach
        </select>
      </div>
      <div class="form-group">
        <label for="">Costo</label><br>
        <input class="form-control" type="number" name="costo" value="costo" step="0.01" required>
      </div>
      <div class="form-group">
        <label for="">Precio de venta</label> <br>
        <input class="form-control" type="number" name="precio" value="precio" step="0.01" required>
      </div>
      <div class="form-group">
        <input class="btn btn-primary" type="submit"  value="Guardar">
        <a class="btn btn-danger" href="{{url('/tecnico/trabajos')}}">Cancelar</div></a
      </div>
    </form>
    @if ($errors->any())
    <div class="alert alert-danger w-60 mh3">
      <h2>Errores en los datos introducidos:</h2>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
  </div>
</div>
@endsection
