@extends('layouts/base')
@section('content')
<div class="row nav-bar">
  <a class="w-third" href="{{url('/tecnico/trabajos')}}"><div class="nav-button w-100 br3 br--left">Trabajos</div></a>
  <a class="w-third" href="{{url('/tecnico/materiales')}}"><div class="nav-button w-100">Materiales</div></a>
  <a class="w-third" href="{{url('/tecnico/clientes')}}"><div class="nav-button-active w-100">Clientes</div></a>
</div>

<div class="row">
  <div class="w-30">
      <h3>{{$client->nombre}} {{$client->apPat}} {{$client->apMat}}</h3>
  </div>
  <div class="w-30" >
    <br>
    <br>
    <h4>Diagnostico</h4>
    <p>{{$diagnostic->diagnostico}}</p>
    <h4>Medidas</h4>
    <table class="table table-striped">
      <thead>
        <tr>
          <!--  -->
          <th>Pie</th>
          <th>Largo de Pie</th>
          <th>Ancho de talon</th>
          <th>Ancho de Arco</th>
          <th>Medida de Boton</th>
        </tr>
      </thead>
      <tbody>
      <tr>
        <td>Izquierdo</td>
        <td>{{$medid->LDpieI}}</td>
        <td>{{$medid->ADTalonI}}</td>
        <td>{{$medid->ADArcoI}}</td>
        <td>{{$medid->MDBotonI}}</td>
      </tr>
      <tr>
        <td>Derecho</td>
        <td>{{$medid->LDpieD}}</td>
        <td>{{$medid->ADTalonD}}</td>
        <td>{{$medid->ADArcoD}}</td>
        <td>{{$medid->MDBotonD}}</td>
      </tr>
      </tbody>
    </table>
    <a href="{{url('/tecnico/clientes')}}" class="btn btn-primary">Regresar</a>
  </div>
</div>

@endsection
