@extends('layouts/base')
@section('content')
<div class="row nav-bar">
  <a class="w-third" href="{{url('/tecnico/trabajos')}}"><div class="nav-button w-100 br3 br--left">Trabajos</div></a>
  <a class="w-third" href="{{url('/tecnico/materiales')}}"><div class="nav-button w-100">Materiales</div></a>
  <a class="w-third" href="{{url('/tecnico/clientes')}}"><div class="nav-button-active w-100">Clientes</div></a>
</div>
<div class="row">
  <div class="w-100">
    <div id="clientes" class="">
      <div class="w-100 bg-light br3 mv3">
        <a href="{{url('/tecnico/clientes/nuevo_expediente')}}" class="btn btn-danger">+Agregar expediente</a>
      </div>
      <div class="w-100 table-responsive">
        <table class="table table-striped">
          <thead>
            <tr>
              <!--  -->
              <th>#</th>
              <th>Nombre</th>
              <th>E-mail</th>
              <th>Fecha de registro</th>
              <th>Expediente</th>
            </tr>
          </thead>
          <tbody>
            @foreach($diagnosticos as $diagnostico)
            <tr>
              <th scope="row">{{$diagnostico->user->id}}</th>
              <td>{{$diagnostico->user->nombre}} {{$diagnostico->user->apPat}} {{$diagnostico->user->apMat}}</td>
              <td>{{$diagnostico->user->email}}</td>
              <td>{{\Carbon\Carbon::parse($diagnostico->user->emailV)->format('d-m-Y')}}</td>
              <td>
                <div class="w-100">
                  <a class="btn btn-primary"  href="{{url('/tecnico/clientes/expediente/'.$diagnostico->user->id)}}"> Ver expediente</a>
                </div>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection
