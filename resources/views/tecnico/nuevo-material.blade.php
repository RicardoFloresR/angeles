@extends('layouts/base')
@section('content')
<div class="row nav-bar">
  <a class="w-third" href="{{url('/tecnico/trabajos')}}"><div class="nav-button w-100 br3 br--left">Trabajos</div></a>
  <a class="w-third" href="{{url('/tecnico/materiales')}}"><div class="nav-button w-100">Materiales</div></a>
  <a class="w-third" href="{{url('/tecnico/clientes')}}"><div class="nav-button w-100">Clientes</div></a>
</div>
<div class="row">
  <div class="w-40">

  </div>
  <div class="w-60">
    <h3>Nuevo Material</h3>
    <form class="" action="{{url('/tecnico/materiales/agregar_material')}}" method="post">
      {!! csrf_field() !!}
      <div class="form-group w-30">
        <label for="">Nombre</label><br>
        <input class="form-control" type="text" name="nombre" value="">
      </div>
      <div class="form-group w-30">
        <label for="">Correo electronico</label><br>
        <select class="form-control" name="id_proveedor">
          @foreach ($proveedores as $proveedor)
            <option value="{{$proveedor->id}}">{{$proveedor->nombre}}</option>
          @endforeach
        </select>
      </div>
      <div class="form-group w-30">
        <label for="">Stock</label> <br>
        <input class="form-control" type="number" name="stock" step="0.01"value="">
      </div>
      <div class="form-group w-30">
        <label for="">Unidad de medida</label> <br>
        <input class="form-control" type="text" name="unidad_de_medida" value="">
      </div>
      <div class="form-group">
        <input class="btn btn-primary" type="submit"  value="Guardar">
        <a class="btn btn-danger" href="{{url('/tecnico/materiales')}}">Cerrar</div></a>
      </div>
    </form>
    @if ($errors->any())
    <div class="alert alert-danger w-60 mh3">
      <h2>Errores en los datos introducidos:</h2>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
  </div>
</div>

@endsection
