@extends('layouts/base')
@section('content')
<style media="screen">

</style>
</script>
<div class="container text-center ">
  <div id="productos" class="flex-container">
    @foreach ($productos as $producto)
      <div class="producto white-panel w-40">
          <h3>{{$producto->nombre}}</h3>
          <img src="{{$producto->imagen->url}}" width="200">
          <div class="producto-info panel">
              <p>Precio: ${{number_format($producto->precio)}}</p>
              <p>
                <a href="#" class="btn btn-danger"> <i class="fa fa-cart-plus"></i> Agregar al carrito</a>
              </p>
          </div>
      </div>
      @endforeach
  </div>
</div>
@endsection
