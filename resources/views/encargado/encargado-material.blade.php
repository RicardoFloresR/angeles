@extends('layouts/base')
@section('content')
<div class="row nav-bar">
  <a class="w-20" href="{{url('/encargado/general')}}"><div class="nav-button w-100 br3 br--left">General</div></a>
  <a class="w-20" href="{{url('/encargado/productos')}}"><div class="nav-button w-100">Productos</div></a>
  <a class="w-20" href="{{url('/encargado/materiales')}}"><div class="nav-button-active w-100">Materiales</div></a>
  <a class="w-20" href="{{url('/encargado/usuarios')}}"><div class="nav-button w-100">Usuarios</div></a>
  <a class="w-20" href="{{url('/encargado/citas')}}"><div class="nav-button w-100 br3 br--right">Citas</div></a>
</div>
  <div class="row">
    <div class="w-20 br2">
      <button class="tablinks tab-button w-100 active" onclick="openContent(event, 'listado');">Listado</button>
      <button class="tablinks tab-button w-100" onclick="openContent(event, 'proveedores');">Proveedores</button>
    </div>
    <div class="w-80">
      <div id="listado" class="tabcontent" style="display:block;">
        <div class="w-100 bg-light br3 mv3">
          <a href="{{url('/encargado/materiales/agregar')}}" class="btn btn-primary">+Agregar material</a>
        </div>
        <div class="w-100 table-responsive">
          <table class="table table-striped">
            <thead>
              <tr>
                <th>#</th>
                <th>Nombre</th>
                <th>Proveedor</th>
                <th>Stock</th>
                <th>Unidad de medida</th>
                <th>Opciones</th>
              </tr>
            </thead>
            <tbody>
              @foreach($materiales as $material)
              <tr>
                <th scope="row">{{$material->id}}</th>
                <td>{{$material->nombre}}</td>
                <td>{{$material->proveedor->nombre}}</td>
                <td>{{$material->stock}}</td>
                <td>{{$material->unidad_de_medida}}</td>
                <td>
                  <a class="btn btn-primary" href="{{url('/encargado/materiales/'.$material->id.'/editar')}}">Editar</a>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
      <div id="proveedores" class="tabcontent">
        <div class="w-100 bg-light br3 mv3">
          <a href="#" class="btn btn-primary">+Agregar Proveedor</a>
        </div>
        <div class="w-100 table-responsive">
          <table class="table table-striped">
            <thead>
              <tr>
                <th>Codigo</th>
                <th>Nombre</th>
                <th>E-mail de contacto</th>
                <th>Numero de contacto</th>
                <th>Opciones</th>
              </tr>
            </thead>
            <tbody>
              @foreach($proveedores as $proveedor)
              <tr>
                <th>{{$proveedor->id}}</th>
                <td>{{$proveedor->nombre}}</td>
                <td>{{$proveedor->email}}</td>
                <td>{{$proveedor->numero_contacto}}</td>
                <td>
                  <div class="w-100">
                    <a class="btn btn-primary" href="#">Editar</a>
                    <button class="btn btn-danger" type="button" name="button">Eliminar</button>
                  </div>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  @section('modal')
  <div class="modal fade" id="{{'modal'}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title bg-1D5261"  id="exampleModalLongTitle">Nuevo proveedor:</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body flex justify-center">
          <form class="" action="{{url('/tecnico/materiales/nuevo_proveedor')}}" method="post">
            {!! csrf_field() !!}
            <div class="form-group">
              <label for="">Nombre</label><br>
              <input type="text" name="nombre" value="">
            </div>
            <div class="form-group">
              <label for="">Correo electronico</label><br>
              <input type="text" name="email" value="">
            </div>
            <div class="form-group">
              <label for="">Telefono de contacto</label> <br>
              <input type="text" name="numero_contacto" value="">
            </div>
            <div class="form-group">
              <input class="btn btn-primary" type="submit" name="" value="Guardar">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection
@endsection
