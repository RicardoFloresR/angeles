@extends('layouts/base')
@section('content')
<div class="row nav-bar">
  <a class="w-20" href="{{url('/encargado/general')}}"><div class="nav-button-active w-100 br3 br--left">General</div></a>
  <a class="w-20" href="{{url('/encargado/productos')}}"><div class="nav-button w-100">Productos</div></a>
  <a class="w-20" href="{{url('/encargado/materiales')}}"><div class="nav-button w-100">Materiales</div></a>
  <a class="w-20" href="{{url('/encargado/usuarios')}}"><div class="nav-button w-100">Usuarios</div></a>
  <a class="w-20" href="{{url('/encargado/citas')}}"><div class="nav-button w-100 br3 br--right">Citas</div></a>
</div>
  <div class="row">
    <div class="w-20 br2">
      <button class="tablinks tab-button w-100 active" onclick="openContent(event, 'trabajos');">Trabajos</button>
      <button class="tablinks tab-button w-100" onclick="openContent(event, 'estadisticas');">Estadísticas</button>
      <!-- <button class="tablinks tab-button w-100">Solicitudes de registro</button> -->
    </div>
    <div class="w-80">
      <div id="trabajos" class="tabcontent" style="display:block;">
        <div class="w-100 bg-light br3 mv3">
          <a href="{{url('/encargado/general/agregar-trabajo')}}" class="btn btn-primary">+Agregar trabajo</a>
        </div>
        <div class="w-100 table-responsive">
          <div class="ma2 b">Los trabajos terminados no apareceran en esta tabla</div>
          <table class="table table-striped">
            <thead>
              <tr>
                <!--  -->
                <th>#</th>
                <th>Tecnico</th>
                <th>Modelo</th>
                <th>Color</th>
                <th>Material</th>
                <th>Estado</th>
                <th>Costo</th>
                <th>Precio</th>
              </tr>
            </thead>
            <tbody>
              @foreach($trabajos as $trabajo)
              <tr>
                <th scope="row">{{$trabajo->id}}</th>
                <td>{{$trabajo->user->nombre}} {{$trabajo->user->apPat}} {{$trabajo->user->apMat}}</td>
                <td>{{$trabajo->zapato->modelo->nombre}}</td>
                <td>{{$trabajo->zapato->color}}</td>
                <td>{{$trabajo->zapato->material}}</td>
                <td>{{$trabajo->estado->estado}}</td>
                <td>{{$trabajo->costo}}</td>
                <td>{{$trabajo->precio}}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>

      <div id="estadisticas" class="tabcontent">
        <div class="w-100 bg-light br3 mv3">
          <div class="dropdown">
            <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Estadísticas
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <button class="dropdown-item" type="button" name="button" onclick="openSubcontent(event,'ventas');">Ventas</button>
              <button class="dropdown-item" type="button" name="button" onclick="openSubcontent(event,'compras');">Compras</button>
              <button class="dropdown-item" type="button" name="button" onclick="openSubcontent(event,'ganancias');">Ganancias</button>
            </div>
          </div>
        </div>
        <div class="w-100">
          <div id="ventas" class="subTabcontent">
            uno
          </div>
          <div id="compras" class="subTabcontent">
            dos
          </div>
          <div id="ganancias" class="subTabcontent">
            tres
          </div>
        </div>
      </div>
    </div>
  </div>
@section('scripts')
<script type="text/javascript">
  const openSubcontent = (evt,tab) => {
    let subTabcontent, subTablink;
    subTabcontent = document.getElementsByClassName('subTabcontent');
    for (item of subTabcontent){
      item.style.display = "none";
    }
    document.getElementById(tab).style.display = "block";
  }
</script>
@endsection
@endsection
