@extends('layouts/base')
@section('content')
<div class="row nav-bar">
  <a class="w-20" href="{{url('/encargado/general')}}"><div class="nav-button w-100 br3 br--left">General</div></a>
  <a class="w-20" href="{{url('/encargado/productos')}}"><div class="nav-button-active w-100">Productos</div></a>
  <a class="w-20" href="{{url('/encargado/materiales')}}"><div class="nav-button w-100">Materiales</div></a>
  <a class="w-20" href="{{url('/encargado/usuarios')}}"><div class="nav-button w-100">Usuarios</div></a>
  <a class="w-20" href="{{url('/encargado/citas')}}"><div class="nav-button w-100 br3 br--right">Citas</div></a>
</div>
  <div class="row">
    <div class="w-20 br2">
      <button class="tablinks tab-button w-100 active" onclick="openContent(event, 'listado');">Productos - General</button>
      <button class="tablinks tab-button w-100" onclick="openContent(event, 'zapatos');">Zapatos</button>
      <!-- <button class="tablinks tab-button w-100" onclick="openContent(event, 'estadisticas');">Agregar producto</button> -->
    </div>
    <div class="w-80">
      <div id="listado" class="tabcontent" style="display: block;">
        <div class="w-100 bg-light br3 mv3">
          <a href="{{url('/encargado/productos/nuevo')}}" class="btn btn-primary">+Agregar producto</a>
        </div>
        <div class="w-100 table-responsive">
          <table class="table table-striped">
            <thead>
              <tr>
                <th>Codigo</th>
                <th>Nombre</th>
                <th>Costo</th>
                <th>Precio venta</th>
                <th>Stock</th>
                <th>Opciones</th>
                <!-- <th>Opciones</th> -->
              </tr>
            </thead>
            <tbody>
              @foreach($productos as $producto)
              <tr>
                <th>{{$producto->id}}</th>
                <td>{{$producto->nombre}}</td>
                <td>{{$producto->costo}}</td>
                <td>{{$producto->precio}}</td>
                <td>{{$producto->stock}}</td>
                <!-- <td>Test</td> -->
                <td>
                  <div class="w-100">
                    <button class="btn btn-warning" type="button" name="button" data-toggle="modal" data-target="{{'#modal'.$producto->id}}">Ver imagen</button>
                    <a class="btn btn-primary" href="{{url('/encargado/productos/'.$producto->id.'/editar')}}">Editar</a>
                    <button class="btn btn-danger" type="button" name="button"  data-toggle="modal" data-target="{{'#modal3'.$producto->id}}">Eliminar</button>
                  </div>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
      <div id="zapatos" class="tabcontent">
          <div class="w-100 bg-light br3 mv3">
            <button type="button" class="btn btn-primary" name="button"data-toggle="modal" data-target="#modal_modelo">+Agregar modelo</button>
            <a href="{{url('/encargado/productos/nuevo-zapato')}}" class="btn btn-primary">+Agregar zapato</a>
          </div>
            <div class="w-100 table-responsive">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>Codigo</th>
                    <th>Modelo</th>
                    <th>Material</th>
                    <th>Color</th>
                    <th>Opciones</th>
                    <!-- <th>Opciones</th> -->
                  </tr>
                </thead>
                <tbody>
                  @foreach($zapatos as $zapato)
                  <tr>
                    <th>{{$zapato->id}}</th>
                    <td>{{$zapato->modelo->nombre}}</td>
                    <td>{{$zapato->material}}</td>
                    <td>{{$zapato->color}}</td>
                    <!-- <td>Test</td> -->
                    <td>
                      <div class="w-100">
                        <button class="btn btn-warning" type="" name="button" data-toggle="modal" data-target="{{'#modal2'.$zapato->id}}">Ver imagen</button>
                        <a class="btn btn-primary" href="{{url('/encargado/productos/zapato/'.$zapato->id.'/editar')}}">Editar</a>
                        <button class="btn btn-danger" type="button" name="button"  data-toggle="modal" data-target="{{'#modal4'.$zapato->id}}">Eliminar</button>
                      </div>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
      </div>
    </div>
  </div>
@endsection
@section('modal')

@foreach($productos as $producto)
<div class="modal fade" id="{{'modal'.$producto->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title bg-1D5261"  id="exampleModalLongTitle">Imagen asignada a: {{$producto->nombre}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body flex justify-center">
        <img src="{{asset($producto->imagen->url)}}" height="300" alt="">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
@endforeach

@foreach($zapatos as $zapato)
<div class="modal fade" id="{{'modal2'.$zapato->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title bg-1D5261"  id="exampleModalLongTitle">{{'Imagen asignada a "'.$zapato->modelo->nombre.' '.$zapato->color.'"'}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body flex justify-center">
        <img src="{{asset($zapato->imagen->url)}}" height="300" alt="">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

@endforeach

@foreach($productos as $producto)
<div class="modal fade" id="{{'modal3'.$producto->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title dark-red b" id="exampleModalLongTitle">¡Advertecia!</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ¿Estás seguro de querer eliminar el producto <span class="b">{{$producto->nombre}}</span>? <br>
        Todos los datos sobre su stock, precio y demás serán borrados.
      </div>
      <div class="modal-footer">
        <form class="" action="{{url('/encargado/productos/'.$producto->id.'/delete')}}" method="post">
          {!! csrf_field() !!}
          <input type="submit" class="btn btn-danger" name="" value="Aceptar">
        </form>
        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>
@endforeach

<div class="modal fade" id="modal_modelo" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">+Agregar modelo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="" action="{{url('encargado/productos/add_model')}}" method="post">
          {!! csrf_field() !!}
          <div class="form-group">
            <label for="">Nombre:</label>
            <input type="text" class="form-control" name="nombre" value="" placeholder="Introduzca el nombre del modelo" required>
          </div>
      </div>
      <div class="modal-footer">
          <input type="submit" class="btn btn-primary" name="" value="Agregar">
        </form>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>

@foreach($zapatos as $zapato)
<div class="modal fade" id="{{'modal4'.$zapato->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title dark-red b" id="exampleModalLongTitle">¡Advertecia!</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ¿Estás seguro de querer eliminar el zapato <span class="b">{{$zapato->modelo->nombre}} {{$zapato->color}}</span>? <br>
        Todos los datos sobre su stock, precio y demás serán borrados.
      </div>
      <div class="modal-footer">
        <form class="" action="{{url('/encargado/productos/'.$zapato->id.'/delete_shoe')}}" method="post">
          {!! csrf_field() !!}
          <input type="submit" class="btn btn-danger" name="" value="Aceptar">
        </form>
        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>
@endforeach
@endsection
