@extends('layouts/base')
@section('content')
<div class="row nav-bar">
  <a class="w-20" href="{{url('/encargado/general')}}"><div class="nav-button w-100 br3 br--left">General</div></a>
  <a class="w-20" href="{{url('/encargado/productos')}}"><div class="nav-button w-100">Productos</div></a>
  <a class="w-20" href="{{url('/encargado/materiales')}}"><div class="nav-button w-100">Materiales</div></a>
  <a class="w-20" href="{{url('/encargado/usuarios')}}"><div class="nav-button-active w-100">Usuarios</div></a>
  <a class="w-20" href="{{url('/encargado/citas')}}"><div class="nav-button w-100 br3 br--right">Citas</div></a>
</div>
  <div class="row">
    <div class="w-20 br2">
      <button class="tablinks tab-button w-100 active" onclick="openContent(event, 'clientes');">Clientes</button>
      <button class="tablinks tab-button w-100" onclick="openContent(event, 'empleados');">Empleados</button>
      <button class="tablinks tab-button w-100" onclick="openContent(event, 'solicitudes')">Solicitudes de registro</button>
    </div>
    <div class="w-80">
      <div id="clientes" class="tabcontent" style="display: block;">
        <div class="w-100 bg-light br3 mv3">
          <a href="#" class="btn btn-primary">+Agregar clientes</a>
        </div>
        <div class="w-100 table-responsive">
          <table class="table table-striped">
            <thead>
              <tr>
                <!--  -->
                <th>#</th>
                <th>Nombre</th>
                <th>Correo Electronico</th>
                <th>Fecha de registro</th>
                <th>Opciones</th>
              </tr>
            </thead>
            <tbody>
              @foreach($clientes as $cliente)
              <tr>
                <th scope="row">{{$cliente->id}}</th>
                <td>{{$cliente->nombre}} {{$cliente->apPat}} {{$cliente->apMat}}</td>
                <td>{{$cliente->email}}</td>
                <td>{{\Carbon\Carbon::parse($cliente->emailV)->format('d-m-Y')}}</td>
                <td>
                  <div class="w-100">
                    <a class="btn btn-primary" href="#">Ver Expediente</a>
                    <button class="btn btn-danger" type="button" name="button">Eliminar</button>
                  </div>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>

      <div id="empleados" class="tabcontent">
        <div class="w-100 bg-light br3 mv3">
          <a href="#" class="btn btn-primary">+Agregar empleados</a>
        </div>
        <div class="w-100 table-responsive">
          <table class="table table-striped">
            <thead>
              <tr>
                <!--  -->
                <th>#</th>
                <th>Nombre</th>
                <th>Correo electronico</th>
                <th>Tipo de empleado</th>
                <th>Fecha de registro</th>
                <th>Opciones</th>
              </tr>
            </thead>
            <tbody>
              @foreach($empleados as $empleado)
              <tr>
                <th scope="row">{{$empleado->id}}</th>
                <td>{{$empleado->nombre}} {{$empleado->apPat}} {{$empleado->apMat}}</td>
                <td>{{$empleado->email}}</td>
                <td>{{$empleado->rol->nombre}}</td>
                <td>{{\Carbon\Carbon::parse($empleado->emailV)->format('d-m-Y')}}</td>
                <td>
                  <div class="w-100">
                    <a class="btn btn-primary" href="#">Editar</a>
                    <button class="btn btn-danger" type="button" name="button">Eliminar</button>
                  </div>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
      <div id="solicitudes" class="tabcontent">
        <div class="w-100 table-responsive">
          <table class="table table-striped">
            <thead>
              <tr>
                <!--  -->
                <th>#</th>
                <th>Nombre</th>
                <th>Correo electronico</th>
              </tr>
            </thead>
            <tbody>
              @foreach($solicitudes as $solicitud)
              <tr>
                <th scope="row">{{$solicitud->id}}</th>
                <td>{{$solicitud->nombre}} {{$solicitud->apPat}} {{$solicitud->apMat}}</td>
                <td>{{$solicitud->email}}</td>
                <td>
                  <div class="w-100">
                    <button class="btn btn-primary" type="button"name="button" data-toggle="modal" data-target="{{'#modal'.$solicitud->id}}">Verificar</a>
                    <button class="btn btn-danger" type="button" name="button">Eliminar</button>
                  </div>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@section('modal')
@foreach($solicitudes as $solicitud)
<div class="modal fade" id="{{'modal'.$solicitud->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title bg-1D5261"  id="exampleModalLongTitle">{{'Verificacion de Usuario: '.$solicitud->email }}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body flex justify-center">
          <form class="" action="{{url('/encargado/usuarios/verificar_tipo/'.$solicitud->id)}}" method="post">
          {!! csrf_field() !!}
            <div class="form-group">
              <label for="id_rol">Tipo de Usuario:</label> <br>
                <!-- <input type="radio" name="id_rol" value="0">Cliente <br>
                <input type="radio" name="id_rol" value="1">Recepcionista <br>
                <input type="radio" name="id_rol" value="2">Tecnico Ortopedista <br>
                <input type="radio" name="id_rol" value="3">Encargado <br>-->
                <select class="form-control" name="id_rol" >
                  <option value="1">Cliente</option>
                  <option value="2">Recepcionista </option>
                  <option value="3">Tecnico Ortopedista</option>
                </select>
            </div>
            <div class="form-group">
              <input class="btn btn-primary" type="submit" name="" value="Guardar">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
            </div>
          </form>
      </div>
    </div>
  </div>
</div>

@endforeach
@endsection
@section('scripts')
<script type="text/javascript">
  const openSubcontent = (evt,tab) => {
    let subTabcontent, subTablink;
    subTabcontent = document.getElementsByClassName('subTabcontent');
    for (item of subTabcontent){
      item.style.display = "none";
    }
    document.getElementById(tab).style.display = "block";
  }
</script>
@endsection
@endsection
