@extends('layouts/base')
@section('content')
<div class="row nav-bar">
  <a class="w-20" href="{{url('/encargado/general')}}"><div class="nav-button w-100 br3 br--left">General</div></a>
  <a class="w-20" href="{{url('/encargado/productos')}}"><div class="nav-button w-100">Productos</div></a>
  <a class="w-20" href="{{url('/encargado/materiales')}}"><div class="nav-button w-100">Materiales</div></a>
  <a class="w-20" href="{{url('/encargado/usuarios')}}"><div class="nav-button-active w-100">Usuarios</div></a>
  <a class="w-20" href="{{url('/encargado/citas')}}"><div class="nav-button w-100 br3 br--right">Citas</div></a>
</div>
  <div class="row">
    <div class="w-100">
      <div id="listado" class="">
        <div class="w-100 bg-light br3 mv3">
          <a href="#" class="btn btn-primary">+Agregar nueva cita</a>
        </div>
        <div class="w-100 table-responsive">
          <table class="table table-striped">
            <thead>
              <tr>
                <th>Codigo</th>
                <th>Nombre de cliente</th>
                <th>Dia</th>
                <th>Hora</th>
                <th>Estado</th>
                <th>Acciones</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($citas as $cita)
              <tr>
                <th>1</th>
                <td>{{$cita->usuario->nombre}} {{$cita->usuario->apPat}} {{$cita->usuario->apMat}}</td>
                <td>{{\Carbon\Carbon::parse($cita->fecha)->format('d-m-Y')}}</td>
                <td>{{\Carbon\Carbon::parse($cita->fecha)->format('h:i')}}</td>
                <td>{{$cita->estado}}</td>
                <td>
                  @if ($cita->estado == 'Por confirmar')
                  <button class="btn btn-primary" type="button"name="button" data-toggle="modal" data-target="{{'#modal'.$cita->id}}">Confirmar</button>
                  <button class="btn btn-danger" type="button"name="button" data-toggle="modal" data-target="{{'#modal2'.$cita->id}}">Eliminar</button>
                  <button class="btn btn-warning" type="button" name="button" href="#">Editar</button>
                  @elseif ($cita->estado == 'Confirmada')
                  <button class="btn btn-danger" type="button"name="button" data-toggle="modal" data-target="{{'#modal2'.$cita->id}}">Eliminar</button>
                  <button class="btn btn-warning" type="button" name="button" href="#">Editar</button>
                  @endif
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  @section('modal')
  @foreach($citas as $citas)
  <div class="modal fade" id="{{'modal'.$cita->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title bg-1D5261"  id="exampleModalLongTitle">Confirmar Cita: {{$cita->usuario->nombre}} {{$cita->usuario->apPat}} {{$cita->usuario->apMat}}</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body flex justify-center">
            <form class="" action="{{url('/encargadp/citas/confirmar/'.$cita->id)}}" method="post">
            {!! csrf_field() !!}
              <div class="form-group">
                  <h4>Desea confirmar esta cita?</h4>
              </div>
              <div class="form-group">
                <input class="btn btn-primary" type="submit" name="" value="Aceptar">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
              </div>
            </form>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="{{'modal2'.$cita->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title bg-1D5261"  id="exampleModalLongTitle">Eliminar Cita: {{$cita->usuario->nombre}} {{$cita->usuario->apPat}} {{$cita->usuario->apMat}}</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body flex justify-center">
            <form class="" action="{{url('/encargado/citas/eliminar/'.$cita->id)}}" method="post">
            {!! csrf_field() !!}
              <div class="form-group">
                  <h4>Esta seguro que desea eliminar esta cita?</h4>
              </div>
              <div class="form-group">
                <input class="btn btn-primary" type="submit" name="" value="Aceptar">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
              </div>
            </form>
        </div>
      </div>
    </div>
  </div>
  @endforeach
  @endsection
@endsection
