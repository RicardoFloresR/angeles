@extends('layouts/base')
@section('content')
<div class="row nav-bar">
  <a class="w-20" href="{{url('/encargado/general')}}"><div class="nav-button w-100 br3 br--left">General</div></a>
  <a class="w-20" href="{{url('/encargado/productos')}}"><div class="nav-button w-100">Productos</div></a>
  <a class="w-20" href="{{url('/encargado/materiales')}}"><div class="nav-button-active w-100">Materiales</div></a>
  <a class="w-20" href="{{url('/encargado/usuarios')}}"><div class="nav-button w-100">Usuarios</div></a>
  <a class="w-20" href="{{url('/encargado/citas')}}"><div class="nav-button w-100 br3 br--right">Citas</div></a>
</div>
<div class="row">
  <div class="w-40">
  </div>
  <div class="w-60">
    <h3>Nuevo Material</h3>
    <form class="" action="{{url('/encargado/materiales/agregar_material')}}" method="post">
      {!! csrf_field() !!}
      <div class="form-group w-30">
        <label for="">Nombre</label><br>
        <input type="text" name="nombre" value="">
      </div>
      <div class="form-group w-30">
        <label for="">Correo electronico</label><br>
        <select class="form-control" name="id_proveedor">
          @foreach ($proveedores as $proveedor)
            <option value="{{$proveedor->id}}">{{$proveedor->nombre}}</option>
          @endforeach
        </select>
      </div>
      <div class="form-group w-30">
        <label for="">Stock</label> <br>
        <input type="number" name="stock" step="0.01"value="">
      </div>
      <div class="form-group w-30">
        <label for="">Unidad de medida</label> <br>
        <input type="text" name="unidad_de_medida" value="">
      </div>
      <div class="form-group">
        <input class="btn btn-primary" type="submit"  value="Guardar">
        <a class="btn btn-danger" href="{{url('/encargado/materiales')}}">Cerrar</div></a>
      </div>
    </form>
    @if ($errors->any())
    <div class="alert alert-danger w-60 mh3">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
  </div>
</div>

@endsection
