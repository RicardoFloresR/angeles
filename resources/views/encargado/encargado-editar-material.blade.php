@extends('layouts/base')
@section('content')
<div class="row nav-bar">
  <a class="w-20" href="{{url('/encargado/general')}}"><div class="nav-button w-100 br3 br--left">General</div></a>
  <a class="w-20" href="{{url('/encargado/productos')}}"><div class="nav-button w-100">Productos</div></a>
  <a class="w-20" href="{{url('/encargado/materiales')}}"><div class="nav-button-active w-100">Materiales</div></a>
  <a class="w-20" href="{{url('/encargado/usuarios')}}"><div class="nav-button w-100">Usuarios</div></a>
  <a class="w-20" href="{{url('/encargado/citas')}}"><div class="nav-button w-100 br3 br--right">Citas</div></a>
</div>
<div class="w-100 flex">
  <div class="w-60">
    <div class="w-100">
      <h2>Datos del Material:</h2>
    </div>
    <form class="row" action="{{url('encargado/materiales/'.$material->id.'/edit_material')}}" method="post">
      {!! csrf_field() !!}
      <div class="form-group w-50 ph2">
        <label for="">Nombre:</label>
        <input type="text" name="nombre" class="form-control" name="" value="{{old('nombre', $material->nombre)}}" required>
      </div>
      <div class="form-group w-50 ph2">
        <label for="">Proveedor:</label>
        <select class="form-control" name="id_proveedor">
          @foreach($proveedores as $proveedor)
          @if($material->id_proveedor == $proveedor->id)
          <option value="{{$proveedor->id}}" selected>{{$proveedor->nombre}}</option>
          @else
          <option value="{{$proveedor->id}}">{{$proveedor->nombre}}</option>
          @endif
          @endforeach
        </select>
      </div>
      <div class="form-group w-50 ph2">
        <label for="">Stock:</label> <span class="f6">No tienes permisos para editar este campo</span>
        <input type="number" class="form-control" disabled="true" name="" value="{{$material->stock}}">
      </div>
      <div class="form-group w-50 ph2">
        <label for="">Unidad de medida:</label>
        <input type="text" name="unidad_de_medida" class="form-control" name="" value="{{old('unidad_de_medida', $material->unidad_de_medida)}}" required>
      </div>
      <div class="w-100 ph2" align="right">
        <input type="submit" id="submitButton" class="btn btn-primary" name="" value="Guardar cambios">
        <a class="btn btn-danger" href="{{url('/encargado/materiales')}}">Cancelar</a>
      </div>
    </form>
  </div>
  @if ($errors->any())
  <div class="alert alert-danger w-30 mh3">
    <h2>Errores en los datos introducidos:</h2>
      <ul>
          @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
          @endforeach
      </ul>
  </div>
  @endif
</div>
@endsection
