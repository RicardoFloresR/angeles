@extends('layouts/base')
@section('content')
<div class="row nav-bar">
  <a class="w-20" href="{{url('/encargado/general')}}"><div class="nav-button w-100 br3 br--left">General</div></a>
  <a class="w-20" href="{{url('/encargado/productos')}}"><div class="nav-button-active w-100">Productos</div></a>
  <a class="w-20" href="{{url('/encargado/materiales')}}"><div class="nav-button w-100">Materiales</div></a>
  <a class="w-20" href="{{url('/encargado/usuarios')}}"><div class="nav-button w-100">Usuarios</div></a>
  <a class="w-20" href="{{url('/encargado/citas')}}"><div class="nav-button w-100 br3 br--right">Citas</div></a>
</div>
<div class="w-100 flex">
  <div class="w-60">
    <div class="w-100">
      <h2>Datos del zapato:</h2>
    </div>
    <form class="row" action="{{url('encargado/productos/'.$zapato->id.'/edit_shoe')}}" method="post" enctype="multipart/form-data">
      {!! csrf_field() !!}
      <div class="form-group w-50 ph2">
        <label for="">Modelo:</label>
        <select class="form-control" name="modelo_id">
          @foreach($modelos as $modelo)
          @if($modelo->id == $zapato->modelo->id)
          <option value="{{$modelo->id}}" selected>{{$modelo->nombre}}</option>
          @endif
          <option value="{{$modelo->id}}">{{$modelo->nombre}}</option>
          @endforeach
        </select>
      </div>
      <div class="form-group w-50 ph2">
        <label for="">Material:</label>
        <input type="text" name="material" class="form-control" name="" value="{{old('material',$zapato->material)}}" required>
      </div>
      <div class="form-group w-100 ph2">
        <label for="">Color:</label>
        <input type="text" name="color" class="form-control" name="" value="{{old('color',$zapato->color)}}" required>
      </div> <br>
      <div class="w-50 ph2">
        <label for="">Imagen:</label>
        <div class="custom-file mb1">
          <input type="file" name="imagen" class="custom-file-input" id="product-img" accept="image/*" required>
          <label class="custom-file-label" id="img-name" for="customFile">Seleccione una nueva imagen para el producto</label>
        </div>
          <img class="img-preview" src="{{asset($zapato->imagen->url)}}" id="img-preview" />
      </div>
      <div class="w-50 ph2" align="right">
        <input type="submit" id="submitButton" class="btn btn-primary" name="" value="Guardar cambios">
        <a class="btn btn-danger" href="{{url('/encargado/productos')}}">Cancelar</a>
      </div>
    </form>
  </div>
  @if ($errors->any())
  <div class="alert alert-danger w-30 mh3">
    <h2>Errores en los datos introducidos:</h2>
      <ul>
          @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
          @endforeach
      </ul>
  </div>
  @endif
</div>
@endsection
@section('scripts')
<script type="text/javascript">
   let imgP = document.getElementById('img-preview');
   function readURL(input) {
       if (input.files && input.files[0]) {
           var reader = new FileReader();

           reader.onload = function (e) {
               $('#img-preview').attr('src', e.target.result);
           }
           reader.readAsDataURL(input.files[0]);
           imgP.style.borderRadius = "10px";
           imgP.style.marginTop = "10px";
           imgP.style.marginBottom = "10px";
           imgP.style.maxHeight = "300px";
       }
   }
   $("#product-img").change(function(){
       readURL(this);
   });

   let name;
   $(document).ready(function(){
    $('#product-img').on('change', function(event) {
        name = event.target.files[0].name;
        document.getElementById('img-name').innerHTML = name;
        });
    });
</script>
@endsection
