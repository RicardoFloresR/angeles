@extends('layouts/base')
@section('content')
<div class="row nav-bar">
  <a class="w-20" href="{{url('/encargado/general')}}"><div class="nav-button-active w-100 br3 br--left">General</div></a>
  <a class="w-20" href="{{url('/encargado/productos')}}"><div class="nav-button w-100">Productos</div></a>
  <a class="w-20" href="{{url('/encargado/materiales')}}"><div class="nav-button w-100">Materiales</div></a>
  <a class="w-20" href="{{url('/encargado/usuarios')}}"><div class="nav-button w-100">Usuarios</div></a>
  <a class="w-20" href="{{url('/encargado/citas')}}"><div class="nav-button w-100 br3 br--right">Citas</div></a>
</div>
<div class="row">
  <div class="w-100 flex">
    <form class="w-40" action="{{url('/encargado/general/new_work')}}" method="post">
      <h3>Nuevo trabajo</h3>
      {!! csrf_field() !!}
      <div class="form-group">
        <label for="">Cliente:</label><br>
        <select class="form-control" name="id_medida">
          <option value="">Seleccione un cliente</option>
          @foreach ($medidas as $medida)
            <option value="{{$medida->id}}">{{$medida->user->nombre}} {{$medida->user->apPat}} {{$medida->user->apMat}}</option>
          @endforeach
        </select>
      </div>
      <div class="form-group">
        <label for="">Técnico:</label><br>
        <select class="form-control" name="id_tecnico">
          <option value="">Seleccione un técnico</option>
          @foreach ($tecnicos as $tecnico)
            <option value="{{$tecnico->id}}">{{$tecnico->nombre}} {{$tecnico->apPat}} {{$tecnico->apMat}}</option>
          @endforeach
        </select>
      </div>
      <div class="form-group">
        <label for="">Modelo de zapato:</label><br>
        <select class="form-control" name="id_zapato">
          <option value="">Seleccione un modelo</option>
          @foreach ($zapatos as $zapato)
            <option value="{{$zapato->id}}">{{$zapato->modelo->nombre}} {{$zapato->color}} {{$zapato->material}}</option>
          @endforeach
        </select>
      </div>
      <div class="form-group">
        <label for="">Costo</label><br>
        <input class="form-control" type="number" name="costo" value="{{old('costo')}}" step="0.01" placeholder="MXN. Moneda nacional" required>
      </div>
      <div class="form-group">
        <label for="">Precio de venta</label> <br>
        <input class="form-control" type="number" name="precio" value="{{old('precio')}}" step="0.01" placeholder="MXN. Moneda nacional" required>
      </div>
      <div class="form-group">
        <input class="btn btn-primary" type="submit"  value="Guardar">
        <a class="btn btn-danger" href="{{url('/encargado/general')}}">Cancelar</a
      </div>
    </form>
    @if ($errors->any())
    <div class="alert alert-danger w-60 mh3">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
  </div>
</div>
@endsection
