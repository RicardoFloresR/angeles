@extends('layouts/base')
@section('content')
<div class="row nav-bar">
  <a class="w-20" href="{{url('/encargado/general')}}"><div class="nav-button w-100 br3 br--left">General</div></a>
  <a class="w-20" href="{{url('/encargado/productos')}}"><div class="nav-button-active w-100">Productos</div></a>
  <a class="w-20" href="{{url('/encargado/materiales')}}"><div class="nav-button w-100">Materiales</div></a>
  <a class="w-20" href="{{url('/encargado/usuarios')}}"><div class="nav-button w-100">Usuarios</div></a>
  <a class="w-20" href="{{url('/encargado/citas')}}"><div class="nav-button w-100 br3 br--right">Citas</div></a>
</div>
<div class="row">
  <div class="w-100 flex">
    <form class="w-40" action="{{url('/encargado/productos/new_shoe')}}" method="post" enctype="multipart/form-data">
      <h3>Nuevo Zapato</h3>
      {!! csrf_field() !!}
      <div class="form-group">
        <label for="">Modelo:</label><br>
        <select class="form-control" name="modelo">
          <option value="">Seleccione un modelo.</option>
          @foreach($modelos as $modelo)
          <option value="{{$modelo->id}}">{{$modelo->nombre}}</option>
          @endforeach
        </select>
      </div>
      <div class="form-group">
        <label for="">Material:</label><br>
        <input class="form-control" type="text" name="material" value="{{old('material')}}" step="0.01" placeholder="Introduzca el nombre del material."  required>
      </div>
      <div class="form-group">
        <label for="">Color:</label><br>
        <input class="form-control" type="text" name="color" value="{{old('color')}}" step="0.01" placeholder="Introduzca el color del zapato."  required>
      </div>
      <label for="">Imagen:</label>
      <div class="custom-file mb1">
        <input type="file" name="imagen" class="custom-file-input" id="product-img" accept="image/*" required>
        <label class="custom-file-label" id="img-name" for="customFile">Seleccione una imagen para el producto</label>
      </div>
        <img class="img-preview" src="" id="img-preview" />
      <div class="form-group">
        <input class="btn btn-primary" type="submit"  value="Guardar">
        <a class="btn btn-danger" href="{{url('/encargado/productos')}}">Cancelar</a>
      </div>
    </form>
    @if ($errors->any())
    <div class="alert alert-danger w-60 mh3">
      <h2>Errores en los datos introducidos:</h2>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
  </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
   let imgP = document.getElementById('img-preview');
   function readURL(input) {
       if (input.files && input.files[0]) {
           var reader = new FileReader();

           reader.onload = function (e) {
               $('#img-preview').attr('src', e.target.result);
           }
           reader.readAsDataURL(input.files[0]);
           imgP.style.borderRadius = "10px";
           imgP.style.marginTop = "10px";
           imgP.style.marginBottom = "10px";
       }
   }
   $("#product-img").change(function(){
       readURL(this);
   });

   let name;
   $(document).ready(function(){
    $('#product-img').on('change', function(event) {
        name = event.target.files[0].name;
        document.getElementById('img-name').innerHTML = name;
        });
    });
</script>
@endsection
