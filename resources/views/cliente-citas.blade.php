@extends('layouts/base')
@section('content')
<div class="row nav-bar">
  <a class="w-20" href="{{url('/cliente/home')}}"><div class="nav-button w-100 br3 br--left">Home</div></a>
  <a class="w-20" href="{{url('/cliente/catalogo')}}"><div class="nav-button w-100">Catálogo/Tienda</div></a>
  <a class="w-20" href="{{url('/cliente/ofertas')}}"><div class="nav-button w-100">Ofertas</div></a>
  <a class="w-20" href="{{url('/cliente/citas')}}"><div class="nav-button-active w-100">Citas</div></a>
  <a class="w-20" href="{{url('/cliente/contactanos')}}"><div class="nav-button w-100 br3 br--right">Contactanos</div></a>
</div>
  <div class="row">
    <div class="w-20 br2">
      <button class="tablinks tab-button w-100" onclick="openContent(event, 'verCitas');">Ver Citas</button>
      <button class="tablinks tab-button w-100" onclick="openContent(event, 'solicitar');">Solictar Cita</button>
      <button class="tablinks tab-button w-100" onclick="openContent(event, 'diagnostico');">Consultar Diagnóstico</button>
    </div>
  </div>
      <div class="w-80">
        <div id="verCitas" class="tabcontent">
          <h2>Historial de Citas</h2>
      <div class="w-100 table-responsive">
        <table class="table table-striped">
          <thead>
            <tr>
              <th>Codigo</th>
              <th>Fecha</th>
              <th>Hora</th>
            </tr>
          </thead>
        </table>
      </div>
          </div>
        </div>

        <div class="w-80">
          <div id="solicitar" class="tabcontent">
            <h2>Envianos tu solicitud de Cita</h2>
            <div class="form-group">
              <label for="nombre">Nombre Completo</label>
              <input name="nombre" type="text" class="form-control"placeholder="">
            </div>
            <div class="form-group">
              <label for="apPat">Fecha de Cita (lunes a viernes)</label>
              <input name="fecha" type="date" class="form-control"placeholder="">
            </div>
            <div class="form-group">
              <label for="apMat">Hora de Cita (9:00 a 18:00 hrs)</label>
              <input name="hora" type="time" class="form-control"placeholder="">
            </div>
            <div class="form'group">
              <input class="btn btn-primary" type="submit" name="" value="Enviar Solicitud">
            </div>
          </div>
        </div>


@endsection
