@extends('layouts/base')
@section('content')
<div class="row nav-bar">
  <a class="w-20" href="{{url('/cliente/home')}}"><div class="nav-button w-100 br3 br--left">Home</div></a>
  <a class="w-20" href="{{url('/cliente/catalogo')}}"><div class="nav-button w-100">Catálogo/Tienda</div></a>
  <a class="w-20" href="{{url('/cliente/ofertas')}}"><div class="nav-button-active w-100">Ofertas</div></a>
  <a class="w-20" href="{{url('/cliente/citas')}}"><div class="nav-button w-100">Citas</div></a>
  <a class="w-20" href="{{url('/cliente/contactanos')}}"><div class="nav-button w-100 br3 br--right">Contactanos</div></a>
</div>
@endsection
