@extends('layouts/base')
@section('head')
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
@endsection
@section('content')
<div class="row nav-bar">
  <a class="w-20" href="{{url('/cliente/home')}}"><div class="nav-button-active  w-100 br3 br--left">Home</div></a>
  <a class="w-20" href="{{url('/cliente/catalogo')}}"><div class="nav-button w-100">Catálogo/Tienda</div></a>
  <a class="w-20" href="{{url('/cliente/ofertas')}}"><div class="nav-button w-100">Ofertas</div></a>
  <a class="w-20" href="{{url('/cliente/citas')}}"><div class="nav-button w-100">Citas</div></a>
  <a class="w-20" href="{{url('/cliente/contactanos')}}"><div class="nav-button w-100 br3 br--right">Contactanos</div></a>
</div>
<div class="contenedor" style=" height:600px;">
  <div class="">
    <img class="w-100 slick-img" src="{{asset('img/img1.jpg')}}" alt="">
  </div>
  <div class="">
    <img class="slick-img" src="{{asset('img/img2.jpg')}}" alt="">
  </div>
  <div class="">
    <img class="slick-img" src="{{asset('img/img3.jpg')}}" alt="">
  </div>
</div>
<div class="w-100 row flex justify-around">
  <div class="w-25 h5 mv4"
  style="background-color: white; margin: 1rem 1.5rem;
  border: 0 solid transparent;
  border-radius: 4px;
  box-shadow: 0 2px 4px 2px rgba(0,0,0,.08);">
    <div class="tc b mt3" style="color:#5697A3;">
      <span>INFORMACION</span>
    </div>
    <div class="pa4 tj" style="font-size: 1vw;">
      <a>Dirección: Miguel Hidalgo #34,</a><br>
      <a>Col. San Juan ,</a><br>
      <a>Del. Cuahutemoc, 56624, CDMX</a><br><br>
    <a>  Horarios de atención de Lunes </a><br>
        <a>a Viernes de 9:00 a 18:00 hrs.</a><br>
    </div>
  </div>
  <div class="w-25 h5 mv4"
  style="background-color: white; margin: 1rem 1.5rem;
  border: 0 solid transparent;
  border-radius: 4px;
  box-shadow: 0 2px 4px 2px rgba(0,0,0,.08);">
    <div class="tc b mt3" style="color:#5697A3;">
      <span>POLÍTICAS</span>
    </div>
    <div class="pa4 tj" style="font-size: 1vw;">
      <p class="w3-medium"><a href="/Politica-Devolucion" target="_blank">Políticas de Devolción</a></p>
      <p class="w3-medium"><a href="/Politica-Envio" target="_blank">Políticas de Envíos</a></p>
    </div>
  </div>
  <div class="w-25 h5 mv4"
  style="background-color: white; margin: 1rem 1.5rem;
  border: 0 solid transparent;
  border-radius: 4px;
  box-shadow: 0 2px 4px 2px rgba(0,0,0,.08);">
    <div class="tc b mt3" style="color:#5697A3;">
      <span>PAGOS SEGUROS</span>
    </div>
    <div class="pa4 tj" style="font-size: 1vw;">
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis imperdiet risus eu lorem ultrices, et tincidunt mauris ornare. Vestibulum dictum dapibus tempus.
    </div>
  </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
$('.contenedor').slick({
  autoplay: true,
  arrows: false,
  fade: true,
  draggabe: false,
  pauseOnHover: false,
  autoplaySpeed: 2000
});
});
</script>
@endsection
