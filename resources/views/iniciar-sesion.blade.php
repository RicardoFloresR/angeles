@extends('layouts/base')
@section('head')
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
@section('content')
<div class="d-flex jc-c w-100 row">
  <div class="w3-container w3-content w3-padding-64 w-40" style="max-width:1000px">
      <h2 class="w3-wide w3-center">Inicio de sesión</h2><br>
        <div class="w-center">

    <form class="" action="{{url('/login')}}" method="post">
      {!! csrf_field() !!}
      <div class="form-group" >
      <label for="exampleInputEmail1">Correo electrónico</label>
      <input name="email" type="email" class="form-control"  aria-describedby="emailHelp" placeholder="Enter email">
      {!! $errors->first('email', '<span class="login-error">Email no valido</span>') !!}
      <small  class="form-text text-muted">Jamás compartiremos tu correo electrónico con alguien mas.</small>
      </div>
      <div class="form-group">
        <label for="exampleInputPassword1">Contraseña</label>
        <input name="password" type="password" class="form-control"  placeholder="Password">
        {!! $errors->first('password', '<span class="login-error">Contraseña incorrecta</span>') !!}
      </div>
      <div class="form-group" align="center">
        <br>
        <input class="btn btn-primary" type="submit" name="" value="Iniciar sesión">
      </div>
    </form>
    <div class="form-text text-muted mt4">
      ¿No estás registrado? Regístrate dando click <a href="{{url('/registrarse')}}">aquí</a>.
    </div>
    <div class="form-text text-muted mt2 f5">
      ¿Olvidaste tu contraseña? Recupérala dando click <a href="{{url('/recuperar-contraseña')}}">aquí</a>.
    </div>
  </div>
</div>
</div>
@endsection
