@extends('layouts/base')
@section('content')
<div class="d-flex jc-c w-100 row">
  <div class="w3-container w3-content w3-padding-64 w-40" style="max-width:800px">
      <h2 class="w3-wide w3-center">Registrarse</h2><br>
  <div class="3-center">
  <form class="" action="{{url('/register')}}" method="post">
    {!! csrf_field() !!}
    <div class="form-group"  align="left">
      <label for="nombre" >Nombre</label>
      <input name="nombre" type="text" class="form-control tc" placeholder="Introduzca su nombre" value="{{old('nombre') }}" required>
      {!! $errors->first('nombre', '<span class="login-error">Este campo es obligatorio</span>') !!}
    </div>
    <div class="form-group"  align="left">
      <label for="apPat">Apellido paterno</label>
      <input name="apPat" type="text" class="form-control tc"placeholder="Introduzca su apellido paterno" value="{{old('apPat') }}" required>
      {!! $errors->first('apPat', '<span class="login-error">Este campo es obligatorio</span>') !!}
    </div>
    <div class="form-group"  align="left">
      <label for="apMat">Apellido materno</label>
      <input name="apMat" type="text" class="form-control tc"placeholder="Introduzca su apellido materno" value="{{old('apMat') }}" required>
      {!! $errors->first('apMat', '<span class="login-error">Este campo es obligatorio</span>') !!}
    </div>
    <div class="form-group"  align="left">
      <label for="exampleInputEmail1">Correo electrónico</label>
      <input name="email" type="email" class="form-control tc" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Introduzca su correo electrónico" value="{{old('email') }}" required>
      {!! $errors->first('email', '<span class="login-error">El correo brindado ya está en uso</span>') !!}
    </div>
    <div class="form-group"  align="left">
      <label for="exampleInputPassword1">Contraseña</label>
      <input name="password" type="password" class="form-control tc" id="exampleInputPassword1" placeholder="Introduzca una contraseña" required>
      {!! $errors->first('password', '<span class="login-error">Este campo es obligatorio</span>') !!}
    </div>
    <div class="form'group"  align="center">
      <br><input class="btn btn-primary" type="submit" name="" value="Regitrarse">
      <small id="emailHelp" class="form-text text-muted">Jamás compartiremos tu Información con alguien más.</small>
    </div>
  </form>
  <!-- <div class="form-text text-muted mt4">
    ¿No estás registrado? Regístrate dando click <a href="{{url('/registrarse')}}">aquí</a>
  </div> -->
  </div>
</div>
</div>
@endsection
