﻿<?php

Route::get('/', 'ClientController@welcome');

Route::get('/verificacion', function(){
  return view('verificacion');
});

Route::get('/Contactanos', function(){
  return view('contactanos');
});

Route::get('/Politica-Devolucion', function(){
  return view('politica-devolucion');
});

Route::get('/Politica-Envio', function(){
  return view('politica-envio');
});


Route::get('/cliente/home', function(){
  return view('cliente-home');
});

Route::get('/cliente/contactanos', function(){
  return view('cliente-contactanos');
});

Route::get('/cliente/ofertas', function(){
  return view('cliente-ofertas');
});

Route::get('/cliente/citas', function(){
  return view('cliente-citas');
});

Route::get('/cliente/catalogo', function(){
  return view('cliente-catalogo');
});

Route::get('/', 'ClientController@welcome');;
Route::get('/tecnico', function () {
    return view('tecnico/tecnico-menu');
});



Route::get('/registrarse', function(){
  return view('registrar');
});



Route::get('/iniciar-sesion', 'Auth\LoginController@login_view');


Route::get('/iniciar-sesion', 'Auth\LoginController@login_view');

Route::post('/login', 'Auth\LoginController@login');
Route::get('/registrarse', 'Auth\LoginController@register_view');
Route::post('/register', 'Auth\LoginController@register');
Route::get('/logout', 'Auth\LoginController@logout');
Route::get('/catalogo', 'ClientController@ver_catalogo');

// Route::get('/test', 'ManagerController@test');
Route::get('/editar-perfil', 'Controller@actualizar');
Route::post('/editar-perfil', 'Controller@update');

Route::get('/recuperar-contraseña', 'MessagesController@recuperar_contraseña');
Route::post('/recover_password', 'MessagesController@recover_password');

//
// URLS DE PRUEBA
//
Route::get('/borrar', 'Controller@borrar');
Route::post('/delete', 'Controller@delete');

//
// encargado
//
Route::group(['prefix' => '/encargado'], function(){
  Route::get('/general', 'ManagerController@encargado_general');
  Route::get('/general/agregar-trabajo', 'ManagerController@agregar_trabajo');
  Route::post('/general/new_work', 'ManagerController@new_work');

  Route::group(['prefix' => '/productos'], function(){
    // Productos
    Route::get('/', 'ManagerController@encargado_productos');
    Route::get('/nuevo','ManagerController@agregar_producto');
    Route::post('/new', 'ManagerController@add_product');




    Route::get('/{id}/editar', 'ManagerController@editar_producto')->where('id', '[0-9]+');
    Route::post('/{id}/edit', 'ManagerController@edit_product')->where('id', '[0-9]+');


    Route::get('/{id}/editar', 'ManagerController@editar_producto')->where('id', '[0-9]+');
    Route::post('/{id}/edit', 'ManagerController@edit_product')->where('id', '[0-9]+');
    Route::post('/{id}/delete', 'ManagerController@delete_product')->where('id', '[0-9]+');
    // Zapatos
    Route::post('/add_model', 'ManagerController@add_model');
    Route::get('/nuevo-zapato', 'ManagerController@nuevo_zapato');
    Route::post('/new_shoe', 'ManagerController@new_shoe');
    Route::get('/zapato/{id}/editar', 'ManagerController@editar_zapato')->where('id', '[0-9]+');
    Route::post('/{id}/edit_shoe', 'ManagerController@edit_shoe')->where('id', '[0-9]+');
    Route::post('/{id}/delete_shoe', 'ManagerController@delete_shoe')->where('id', '[0-9]+');

  });
  Route::group(['prefix' => '/materiales'], function(){
    Route::get('/','ManagerController@encargado_material');
    Route::get('/agregar', 'ManagerController@agregar_material');
    Route::get('/{id}/editar', 'ManagerController@editar_material')->where('id','[0-9]+');
    Route::post('/{id}/edit_material', 'ManagerController@edit_material')->where('id', '[0-9]+');
    Route::post('/add', 'ManagerController@add_material');
    Route::get('/id', function(){
      return view('materiales/materiales'); // productos/producto
    });
    Route::get('/id/editar', function(){
      return view('/'); //productos/producto-editar
    });
  });

// Route::get('/material', 'ManagerController@encargado_material');
  Route::group(['prefix' => '/materiales'], function(){
    Route::get('/','ManagerController@encargado_material');

    Route::get('/nuevo_material','ManagerController@nuevo_material');
    Route::post('/agregar_material/','ManagerController@agregar_material');

    Route::get('/{id}', 'ManagerController@test')->where('id', '[0-9]+');


  Route::group(['prefix' => '/citas'], function(){
    Route::get('/','ManagerController@encargado_citas');

    Route::get('/id', function(){
      return view('citas/citas'); // productos/producto
    });
    Route::get('/id/editar', function(){
      return view('/'); //productos/producto-editar
    });
  });
// Route::get('/material', 'ManagerController@encargado_material');
  Route::group(['prefix' => '/usuarios'], function(){
    Route::get('/','ManagerController@encargado_clientes');
    Route::post('/verificar_tipo/{id}', 'ManagerController@verificar_tipo');
    Route::get('/id', function(){
      return view('usuarios/clientes');
    });
    Route::get('/id/editar', function(){
      return view('/');
    });
  });
});
//
// tecnico
//
Route::group(['prefix' => '/tecnico'], function(){
  Route::group(['prefix' => '/trabajos'], function(){
  Route::get('/', 'TechnicianController@tecnico_trabajos');
  Route::post('/cambiar_estado/{id}', 'TechnicianController@cambiar_estado');
  Route::get('/agregar_trabajo', 'TechnicianController@agregar_trabajo');
  Route::post('/nuevo_trabajo', 'TechnicianController@nuevo_trabajo');
  Route::post('/eliminar/{id}', 'TechnicianController@eliminar_trabajo');
  // Route::get('/id', function(){
  //   return view('tecnico/trabajos'); // productos/producto
  // });
  Route::get('/id/editar', function(){
    return view('/'); //productos/producto-editar
  });
  });
Route::group(['prefix' => '/materiales'], function(){
  Route::get('/','TechnicianController@tecnico_material');
  Route::get('/nuevo_material','TechnicianController@nuevo_material');
  Route::post('/agregar_material/','TechnicianController@agregar_material');
  Route::post('/nuevo_proveedor', 'TechnicianController@agregar_proveedor');
  Route::post('/editar_proveedor/{id}', 'TechnicianController@editar_proveedor');
  Route::post('/cambiar_stock/{id}', 'TechnicianController@cambiar_stock');
  Route::post('/remesa/{id}', 'TechnicianController@solicitar_remesa');
  Route::get('/id', function(){
    return view('tecnico/materiales'); // productos/producto
  });
  Route::get('/id/editar', function(){
    return view('/'); //productos/producto-editar
  });
});
Route::group(['prefix' => '/clientes'], function(){
  Route::get('/','TechnicianController@tecnico_clientes');
  Route::get('/expediente/{id}', 'TechnicianController@ver_expediente');
  Route::get('/nuevo_expediente', 'TechnicianController@nuevo_expediente');
  Route::post('/agregar_expediente', 'TechnicianController@agregar_expediente');
  Route::get('/id', function(){
    return view('tecnico/clientes'); // productos/producto
  });
  Route::get('/id/editar', function(){
    return view('/'); //productos/producto-editar
  });
});
});
//
// Recepcionista
//
Route::group(['prefix' => '/recepcionista'], function(){
  Route::group(['prefix' => '/citas'], function(){
  Route::get('/', 'ReceptionistController@recepcionista_citas');
  Route::post('/confirmar/{id}', 'ReceptionistController@confirmar_cita');
  Route::get('/id', function(){
    return view('recepcionista/citas');
  });
  Route::get('/id/editar', function(){
    return view('/');
  });
  });
Route::group(['prefix' => '/compras'], function(){
  Route::get('/','ReceptionistController@recepcionista_compras');
  Route::get('/id', function(){
    return view('recepcionista/recepcionista_compras');
  });
  Route::get('/id/editar', function(){
    return view('/');
  });
});
Route::group(['prefix' => '/ventas'], function(){
  Route::get('/','ReceptionistController@recepcionista_ventas');
  Route::get('/id', function(){
    return view('recepcionista/ventas');
  });
  Route::get('/id/editar', function(){
    return view('/');
  });
});
});
});
