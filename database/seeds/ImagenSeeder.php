<?php

use App\Imagen;
use Illuminate\Database\Seeder;

class ImagenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Imagen::create([
          'url' => 'img/silla1.jpg'
        ]);
        Imagen::create([
          'url' => 'img/rodillera1.jpg'
        ]);
        Imagen::create([
          'url' => 'img/baston1.jpg'
        ]);
        Imagen::create([
          'url' => 'img/muletas1.jpg'
        ]);
        Imagen::create([
          'url' => 'img/negro-oxford.jpg'
        ]);
        Imagen::create([
          'url' => 'img/deportivo-blanco.jpg'
        ]);
        Imagen::create([
          'url' => 'img/botas-caq.jpg'
        ]);
    }
}
