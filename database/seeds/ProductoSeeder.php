<?php

use App\Producto;
use Illuminate\Database\Seeder;

class ProductoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Producto::create([
        'nombre' => 'Silla modelo 1',
        'costo' => 2000,
        'precio' => 3500,
        'stock' => 32,
        'id_imagen' => 1
      ]);
      Producto::create([
        'nombre' => 'Rodillera modelo 1',
        'costo' => 240.90,
        'precio' => 350,
        'stock' => 22,
        'id_imagen' => 2
      ]);
      Producto::create([
        'nombre' => 'Baston modelo 1',
        'costo' => 250,
        'precio' => 450,
        'stock' => 2,
        'id_imagen' => 3
      ]);
      Producto::create([
        'nombre' => 'Muletas modelo 1',
        'costo' => 200,
        'precio' => 345,
        'stock' => 14,
        'id_imagen' => 4
      ]);
    }
}
