<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */

    public function run()
    {
        $this->borrarSeeders([
          'rol',
          'users',
          'medida',
          'modelo',
          'zapato',
          'proveedor',
          'material',
          'compra',
          'cita',
          'tecnico',
          'plantilla',
          'diagnostico',
          'estado',
          'trabajo',
          'venta',
          'tecnico',
          'producto',
          'imagen',
        ]);

        // $this->call(UsersTableSeeder::class);
        $this->call(RolSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(MedidaSeeder::class);
        $this->call(ModeloSeeder::class);
        $this->call(ZapatoSeeder::class);
        $this->call(ProveedorSeeder::class);
        $this->call(MaterialSeeder::class);
        $this->call(CompraSeeder::class);
        $this->call(CitaSeeder::class);
        $this->call(PlantillaSeeder::class);
        $this->call(DiagnosticoSeeder::class);
        $this->call(EstadoSeeder::class);
        $this->call(TrabajoSeeder::class);
        $this->call(VentaSeeder::class);
        $this->call(TecnicoSeeder::class);
        $this->call(ProductoSeeder::class);
        $this->call(ImagenSeeder::class);
    }

    protected function borrarSeeders(array $tables){
      DB::statement('SET FOREIGN_KEY_CHECKS = 0;');
      foreach ($tables as $table) {
        DB::table($table)->truncate();
      }
      DB::statement('SET FOREIGN_KEY_CHECKS = 0;');
    }
}
