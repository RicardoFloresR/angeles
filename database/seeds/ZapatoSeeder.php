<?php

Use App\Zapato;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class ZapatoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Zapato::create([
        // 'id' => '5',
        'id_modelo' => '1',
        'material' => 'Piel',
        'color'=>'Negro',
        'id_imagen' => '5',
        ])  ;
      Zapato::create([
        'id_modelo' => '2',
        'material' => 'Piel',
        'color'=>'Blanco',
        'id_imagen' => '6',
        ])  ;
      Zapato::create([
        'id_modelo' => '3',
        'material' => 'Piel',
        'color'=>'Caqui',
        'id_imagen' => '7',
        ])  ;
    }
}
