<?php

use App\Estado;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class EstadoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Estado::create([
          'estado' => 'Terminado',
        ])  ;
      Estado::create([
          'estado' => 'En Proceso',
        ])  ;
      Estado::create([
          'estado' => 'En espera de material',
        ])  ;
      Estado::create([
          'estado' => 'Iniciado',
        ])  ;
    }
}
