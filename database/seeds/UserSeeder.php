<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
            User::create([
              // 'id' => '5',
              'nombre' => 'Roberta',
              'apPat' => 'Ramirez',
              'apMat' => 'Antonio',
              'email' => 'alvtest@test.com',
              'password' => Hash::make('test'),
              'emailV' => '2019-04-02 00:00:00',
              'id_rol' => '2',
              ]);
              User::create([
                // 'id' => '5',
                'nombre' => 'Axel',
                'apPat' => 'Guzman',
                'apMat' => 'Fuentes',
                'email' => 'tes21t@test.com',
                'password' => Hash::make('test123'),
                'emailV' => '2019-05-02 13:18:01',
                'id_rol' => '3',
                ])  ;
              User::create([
                // 'id' => '5',
                'nombre' => 'Jonatan',
                'apPat' => 'Basilio',
                'apMat' => 'Anaya',
                'email' => 'test345@test.com',
                'password' => Hash::make('876test'),
                'emailV' => '2019-04-02 00:00:00',
                'id_rol' => '4',
                ]);
              User::create([
                // 'id' => '5',
                'nombre' => 'Luis',
                'apPat' => 'Hernandez',
                'apMat' => 'Macino',
                'email' => 'tes765t@test.com',
                'password' => Hash::make('345test'),
                'emailV' => '2019-04-02 00:00:00',
                'id_rol' => '4',
                ]);
                User::create([
                  // 'id' => '5',
                  'nombre' => 'David',
                  'apPat' => 'Guzman',
                  'apMat' => 'Fuentes',
                  'email' => 'eldaves@test.com',
                  'password' => Hash::make('1486325'),
                  'id_rol' => '1',
                  ]);
                  User::create([
                    // 'id' => '5',
                    'nombre' => 'Edgar',
                    'apPat' => 'Martinez',
                    'apMat' => 'De la Rosa',
                    'email' => 'delaR-edg@test.com',
                    'password' => Hash::make('qwerty'),
                    'id_rol' => '1',
                    ]);

    }
}
