<?php

use App\Modelo;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ModeloSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Modelo::create([
        'nombre'=> 'Oxford'
        ])  ;
      Modelo::create([
        'nombre'=> 'Deportivo'
        ])  ;
      Modelo::create([
        'nombre'=> 'Botas'
        ])  ;
      Modelo::create([
        'nombre'=> 'Derbi'
        ])  ;
    }
}
