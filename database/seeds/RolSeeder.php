<?php

use App\Rol;
use Illuminate\Database\Seeder;

class RolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Rol::create([
          'nombre' => 'Cliente',
          'default_url' => '/'
        ]);
        Rol::create([
          'nombre' => 'Recepcionista',
          'default_url' => '/recepcionista/citas'
        ]);
        Rol::create([
          'nombre' => 'Técnico',
          'default_url' => '/tecnico/trabajos'
        ]);
        Rol::create([
          'nombre' => 'Encargado',
          'default_url' => '/encargado/general'
        ]);
    }
}
