<?php

use App\Proveedor;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class ProveedorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Proveedor::create([
        'nombre' => 'Pieles Fernando',
        'email' => 'pieles12345@test.com',
        'numero_contacto' => '58476215',
        ])  ;
      Proveedor::create([
        'nombre' => 'Los Hermanos Russo',
        'email' => 'los_2hermanos@test.com',
        'numero_contacto' => '58465135',
        ])  ;
      Proveedor::create([
        'nombre' => 'Insumos Harmon',
        'email' => 'iHarmon@test.com',
        'numero_contacto' => '17956421',
        ])  ;
    }
}
