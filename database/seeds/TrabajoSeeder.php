<?php

use App\Trabajo;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TrabajoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Trabajo::create([
        'id_tecnico' => '2',
        'id_zapato' => '1',
        'id_medida' => '1',
        'id_estado' => '4',
        'costo' => '450.50',
        'precio' => '630.25',
        ]);
      Trabajo::create([
        'id_tecnico' => '2',
        'id_zapato' => '2',
        'id_medida' => '2',
        'id_estado' => '3',
        'costo' => '550.50',
        'precio' => '630.25',
        ]);
    }
}
