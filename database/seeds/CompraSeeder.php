<?php

use App\Compra;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CompraSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Compra::create([
        'id_proveedor' => '1',
        'id_material' => '1',
        'horaDeCompra' => '2019-04-05 13:23:00',
        'monto'=>'2500',
        ])  ;
      Compra::create([
        'id_proveedor' => '2',
        'id_material' => '2',
        'horaDeCompra' => '2019-04-05 10:12:56',
        'monto'=>'600',
        ])  ;
      Compra::create([
        'id_proveedor' => '3',
        'id_material' => '3',
        'horaDeCompra' => '2019-04-06 09:47:33',
        'monto'=>'2000',
        ])  ;
    }
}
