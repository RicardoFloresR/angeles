<?php

use App\Medida;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MedidaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Medida::create([
        'id_cliente' => '4',
        'LDpieD' => '278',
        'ADTalonD' => '43',
        'ADArcoD' => '35',
        'MDBotonD' => '32',
        'LDpieI' => '278',
        'ADTalonI' => '43',
        'ADArcoI' => '33',
        'MDBotonI' => '31',
        ]);
    }
}
