<?php

use App\Cita;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class CitaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Cita::create([
        'id_cliente' => '4',
        'estado' => 'Por confirmar',
        'fecha' => '2019-04-22 13:30:00',
        ])  ;
    }
}
