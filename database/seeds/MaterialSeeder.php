<?php

use App\Material;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class MaterialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Material::create([
        'id_proveedor' => '1',
        'nombre' => 'piel borrego negro',
        'stock' => '15',
        'unidad_de_medida' => 'm2',
        ])  ;
      Material::create([
        'id_proveedor' => '2',
        'nombre' => 'clavos 1/8',
        'stock' => '20',
        'unidad_de_medida' => 'Caja',
        ])  ;
      Material::create([
        'id_proveedor' => '3',
        'nombre' => 'pegamento',
        'stock' => '12',
        'unidad_de_medida' => 'litro',
        ])  ;
    }
}
