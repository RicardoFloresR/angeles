<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZapatoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zapato', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_modelo');
            $table->string('material');
            $table->string('color');
            $table->unsignedInteger('id_imagen');
            $table->timestamps();

            $table->foreign('id_modelo')->references('id')->on('modelo');
            $table->foreign('id_imagen')->references('id')->on('imagen');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zapato');
    }
}
