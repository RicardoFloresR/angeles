<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('compra', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('id_proveedor');
            $table->unsignedInteger('id_material');
            $table->dateTime('horaDeCompra');
            $table->float('monto');
            $table->timestamps();

            $table->foreign('id_proveedor')->references('id')->on('proveedor');
            $table->foreign('id_material')->references('id')->on('material');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('compra');
    }
}
