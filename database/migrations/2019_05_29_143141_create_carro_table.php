<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Carro extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         Schema::create('carro', function (Blueprint $table) {
             $table->increments('id');
             $table->unsignedInteger('id_cliente');
             $table->unsignedInteger('id_producto');
             $table->integer('cantidad');
             $table->double('subtotal');
             $table->timestamps();

             $table->foreign('id_cliente')->references('id')->on('users');
             $table->foreign('id_producto')->references('id')->on('producto');
         });
     }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
