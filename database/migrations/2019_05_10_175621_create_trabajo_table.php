<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrabajoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trabajo', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_tecnico');
            $table->unsignedInteger('id_zapato');
            $table->unsignedInteger('id_medida');
            $table->float('costo');
            $table->float('precio');
            $table->unsignedInteger('id_estado');
            $table->timestamps();

            $table->foreign('id_tecnico')->references('id')->on('users');
            $table->foreign('id_zapato')->references('id')->on('zapato');
            $table->foreign('id_medida')->references('id')->on('medida');
            $table->foreign('id_estado')->references('id')->on('estado');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trabajo');
    }
}
