<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('users', function (Blueprint $table) {
          $table->increments('id');
          $table->string('nombre');
          $table->string('apPat', 20);
          $table->string('apMat', 20);
          $table->string('email', 50)->unique();
          $table->string('password');
          $table->datetime('emailV')->nullable();
          $table->unsignedInteger('id_rol');
          $table->rememberToken();
          $table->timestamps();

          $table->foreign('id_rol')->references('id')->on('rol');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
