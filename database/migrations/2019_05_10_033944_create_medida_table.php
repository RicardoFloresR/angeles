<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedidaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('medida', function (Blueprint $table) {
          $table->increments('id');
          $table->unsignedInteger('id_cliente');
          $table->float('LDpieD');
          $table->float('ADTalonD');
          $table->float('ADArcoD');
          $table->float('MDBotonD');
          $table->float('LDpieI');
          $table->float('ADTalonI');
          $table->float('ADArcoI');
          $table->float('MDBotonI');
          $table->timestamps();

          $table->foreign('id_cliente')->references('id')->on('users');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medida');
    }
}
